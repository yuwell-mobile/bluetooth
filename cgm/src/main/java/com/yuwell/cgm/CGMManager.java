package com.yuwell.cgm;

import android.content.Context;

import androidx.annotation.NonNull;

import com.yuwell.bluetooth.le.core.YUBleManager;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2021/3/26.
 */
public class CGMManager extends YUBleManager<CGMCallbacks> {

    /**
     * The manager constructor.
     * <p>
     * After constructing the manager, the callbacks object must be set with
     * {@link #setGattCallbacks(BleManagerCallbacks)}.
     * <p>
     * To connect a device, call {@link #connect(BluetoothDevice)}.
     *
     * @param context the context.
     */
    public CGMManager(@android.support.annotation.NonNull Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return null;
    }
}
