package com.yuwell.bluetooth.le.device.respirator;

import android.support.annotation.Nullable;

import java.util.List;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2021/1/25.
 */
public interface RespiratorCallbacks extends BleManagerCallbacks {

    int WLAN_TYPE_2G = 1;
    int WLAN_TYPE_5G = 2;
    int NULL = -1;

    void onWLANBandRead(int type);

    void onWLANResultsRead(@Nullable List<ScanObj> list);

    void onHomeAPRead(int type, int encryptType);

    void onWLANProfileSent(boolean success);

    void onRepeaterStatusRead(boolean success);

    default void onRepeaterStatus(@Nullable ScanObj status) {};
}
