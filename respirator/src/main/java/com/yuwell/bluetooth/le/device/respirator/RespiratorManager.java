package com.yuwell.bluetooth.le.device.respirator;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.rtk.libbtconfigutil.APClass;
import com.rtk.libbtconfigutil.BTConfigUtil;
import com.yuwell.bluetooth.le.core.YUBleManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import no.nordicsemi.android.ble.Request;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Created by Chen on 2021/1/25.
 */
public class RespiratorManager extends YUBleManager<RespiratorCallbacks> {

    private static final String TAG = "RespiratorManager";

    public static int check_HomeAP = 1;
    public static byte[] Check_HomeAP_BSSID;

    public static final int mTTL_count = 5;

    public static final int STATE_BT_QUERY_WLAN_BAND = 0x01;
    public static final int STATE_BT_SCAN_WLAN_2G = 0x02;
    public static final int STATE_BT_RECEIVE_WLAN_2G = 0x03;
    public static final int STATE_BT_SEND_WLAN_PROFILE = 0x04;
    public static final int STATE_BT_QUERY_REPEATER_STATUS = 0x05;

    private static final UUID S5901 = UUID.fromString("00005901-0000-1000-8000-00805f9b34fb");
    private static final UUID S5902 = UUID.fromString("00005902-0000-1000-8000-00805f9b34fb");
    private static final UUID S5903 = UUID.fromString("00005903-0000-1000-8000-00805f9b34fb");
    private static final UUID S5904 = UUID.fromString("00005904-0000-1000-8000-00805f9b34fb");
    private static final UUID S5905 = UUID.fromString("00005905-0000-1000-8000-00805f9b34fb");
    private static final UUID FF01 = UUID.fromString("0000ff01-0000-1000-8000-00805f9b34fb");
    private static final UUID _2A0D = UUID.fromString("00002a0d-0000-1000-8000-00805f9b34fb");


    private static final String CMD_WIFI_CONFIG = "AT$WIFICFG";
    private static BTConfigUtil LIB_CONFIG = new BTConfigUtil();

    private BluetoothGattCharacteristic mConfigCharacteristic;
    private BluetoothGattCharacteristic mDataWriteCharacteristic;
    private BluetoothGattCharacteristic mDataNotifyCharacteristic;

    /* scan AP results */
    private List<ScanObj> mWlanAPList_2G = new ArrayList<>();
    private List<ScanObj> mWlanAPList_5G = new ArrayList<>();


    private int state;
    private int wlan2GOr5G = -1;

    public RespiratorManager(@android.support.annotation.NonNull Context context) {
        super(context);

        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String current_bssid = wifiInfo.getBSSID();
        int current_ip = wifiInfo.getIpAddress();
        String ip = String.format("%d.%d.%d.%d", (current_ip & 0xff),(current_ip >> 8 & 0xff),(current_ip >> 16 & 0xff),(current_ip >> 24 & 0xff));

        if (current_bssid != null && !current_bssid.equals("00:00:00:00:00:00") && !ip.equals("0.0.0.0")) {
            check_HomeAP = 1;
            Check_HomeAP_BSSID = MacToByteArray(current_bssid);
        } else {
            check_HomeAP = 0;
            Check_HomeAP_BSSID = new byte[6];
        }
    }

    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    @Override
    public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
        return super.connectable(device, rssi, scanRecord) && isRespirator(device);
    }

    @NonNull
    @Override
    public Request createBond() {
        return super.createBond();
    }

    public void startWifiConfig() {
        if (mDataWriteCharacteristic != null) {
            writeCharacteristic(mDataWriteCharacteristic, CMD_WIFI_CONFIG.getBytes())
                    .done(device -> queryWLANBand())
                    .enqueue();
        }
    }

    public void queryWLANBand() {
        if (mConfigCharacteristic != null) {
            writeCharacteristic(mConfigCharacteristic, LIB_CONFIG.construct_get_wlan_band_cmd())
                    .done(device -> readConfigData())
                    .enqueue();

             state = STATE_BT_QUERY_WLAN_BAND;
        }
    }

    public void scanWifi(boolean wlan2G) {
        if (mConfigCharacteristic != null) {
            writeCharacteristic(mConfigCharacteristic, wlan2G ?
                    LIB_CONFIG.construct_site_survery_2G_cmd() :
                    LIB_CONFIG.construct_site_survery_5G_cmd())
                    .done(device -> readConfigData())
                    .enqueue();

             state = STATE_BT_SCAN_WLAN_2G;
        }
    }

    private void readConfigData() {
        readCharacteristic(mConfigCharacteristic)
                .with((device, data) -> {
                    byte[] values = data.getValue();
                    if (state == STATE_BT_QUERY_WLAN_BAND) {
                        int ret = LIB_CONFIG.parse_wlan_band_reply(values, values.length);
                        if (ret == 1) {
                            state = -1;

                            int wlan2GEnabled = LIB_CONFIG.get_band_support_2G_result();  // 1 enabled
                            int wlan5GEnabled = LIB_CONFIG.get_band_support_5G_result(); // 1 enabled

                            if (wlan2GEnabled == 1) {
                                mCallbacks.onWLANBandRead(RespiratorCallbacks.WLAN_TYPE_2G);
                                wlan2GOr5G = RespiratorCallbacks.WLAN_TYPE_2G;
                            } else if (wlan5GEnabled == 1) {
                                mCallbacks.onWLANBandRead(RespiratorCallbacks.WLAN_TYPE_5G);
                                wlan2GOr5G = RespiratorCallbacks.WLAN_TYPE_5G;
                            } else {
                                // not band supported
                                mCallbacks.onWLANBandRead(RespiratorCallbacks.NULL);
                            }
                        }
                    } else if (state == STATE_BT_SCAN_WLAN_2G || state == STATE_BT_RECEIVE_WLAN_2G) {
                        int ret = LIB_CONFIG.parse_AP_results_2G_reply_GATT(check_HomeAP, Check_HomeAP_BSSID, values, values.length);
                        Log.d(TAG, "AP results reply: " + ret);
                        if(ret == 1) {
                            state = -1;
                            mCallbacks.onWLANResultsRead(getWlanScanResults(wlan2GOr5G));
                        } else if (ret == 0) {
                            state = STATE_BT_RECEIVE_WLAN_2G;
                            readConfigData();
                        } else {
                            int encrypt = -1;
                            if (ret == 2) {
                                encrypt = 0;
                            } else if (ret == 3) {
                                encrypt = 1;
                            } else if (ret == 4) {
                                encrypt = 2;
                            }
                            mCallbacks.onHomeAPRead(RespiratorCallbacks.WLAN_TYPE_2G, encrypt);
                        }
                    } else if(state == STATE_BT_SEND_WLAN_PROFILE) {
                        state = -1;
                        int ret = LIB_CONFIG.parse_AP_profile_ACK_reply(values, values.length);
                        mCallbacks.onWLANProfileSent(ret == 1);
                    } else if(state == STATE_BT_QUERY_REPEATER_STATUS) {
                        state = -1;
                        int ret = LIB_CONFIG.parse_repeater_status_reply(values, values.length);
                        Log.d(TAG, "STATE_BT_QUERY_REPEATER_STATUS ACK: " + ret);
                        mCallbacks.onRepeaterStatusRead(ret == 1);
                        if (ret == 1) {
                            mCallbacks.onRepeaterStatus(getRepeaterStatusResults());
                        }
                    }
                })
                .enqueue();
    }

    public List<ScanObj> getWlanScanResults(int wlan2GOr5G) {
        mWlanAPList_2G.clear();
        mWlanAPList_5G.clear();

        if (wlan2GOr5G == RespiratorCallbacks.WLAN_TYPE_2G) {

            APClass APs[] = LIB_CONFIG.get_AP_scan_2G_results();
            if(APs == null)
                return mWlanAPList_2G;

            for (int i = 0; i < APs.length; i++) {
                APClass tmpAP = APs[i];
                mWlanAPList_2G.add(new ScanObj(tmpAP.getSSID(), tmpAP.getMac(), tmpAP.getRssi() - 100, tmpAP.getEncrpytType(), mTTL_count, (byte) 0));
            }

            return mWlanAPList_2G;
        } else if (wlan2GOr5G == RespiratorCallbacks.WLAN_TYPE_5G) {
            APClass APs[] = LIB_CONFIG.get_AP_scan_5G_results();
            if (APs == null)
                return mWlanAPList_5G;

            for(int i = 0; i < APs.length; i++){
                APClass tmpAP = APs[i];
                mWlanAPList_5G.add(new ScanObj(tmpAP.getSSID(), tmpAP.getMac(), tmpAP.getRssi() - 100, tmpAP.getEncrpytType(), mTTL_count, (byte) 1));
            }

            return mWlanAPList_5G;
        } else
            return null;
    }

    public void sendAPProfile(byte band, byte encrypt, String ssid, String mac, String pwd) {
        byte[] mAPProfile = constructAPProfile(band, encrypt, ssid, mac, pwd);
        byte[] sendBuff = LIB_CONFIG.construct_AP_profile_cmd(mAPProfile, mAPProfile.length);
        if(sendBuff != null) {
            writeCharacteristic(mConfigCharacteristic, sendBuff)
                .done(device -> readConfigData())
                .fail((device, status) -> {
                    Log.d(TAG, "sendAPProfileRequestFailed: " + status);
                    mCallbacks.onWLANProfileSent(false);
                })
                .enqueue();
        }

        state = STATE_BT_SEND_WLAN_PROFILE;
    }

    public void queryRepeaterStatus(){
        writeCharacteristic(mConfigCharacteristic, LIB_CONFIG.construct_check_repeater_status_cmd())
                .done(device -> readConfigData())
                .enqueue();
        state = STATE_BT_QUERY_REPEATER_STATUS;
    }

    public ScanObj getRepeaterStatusResults() {
        APClass[] extendedAP = LIB_CONFIG.get_repeater_status_results();
        if (extendedAP.length > 0) {
            return new ScanObj(extendedAP[0].getSSID(), extendedAP[0].getMac(), extendedAP[0].getRssi() - 100,
                    extendedAP[0].getEncrpytType(), extendedAP[0].getConnectStatus(), extendedAP[0].getConfigureStatus());
        } else {
            return null;
        }
    }

    private byte[] constructAPProfile(byte band, byte encrypt, String ssid, String mac,String pwd) {
        byte[] mAPProfile = new byte[104];

        byte[] mAPSsid_bytes = ssid.getBytes();
        byte[] mAPMac_bytes = MacToByteArray(mac);

        mAPProfile[0] = band;
        mAPProfile[1] = encrypt;

        System.arraycopy(mAPSsid_bytes, 0, mAPProfile, 2, mAPSsid_bytes.length);
        System.arraycopy(mAPMac_bytes, 0, mAPProfile, 34, mAPMac_bytes.length);
        if(encrypt != 0) {
            byte[] mAPPwd = pwd.getBytes();
            System.arraycopy(mAPPwd, 0, mAPProfile, 40, mAPPwd.length);
        }

        return mAPProfile;
    }

    private static boolean isRespirator(BluetoothDevice device) {
        String name = device.getName();
        return !TextUtils.isEmpty(name) && (name.startsWith("BC_PAP") || name.contains("Ameba"));
    }

    private static byte[] MacToByteArray(String hex_str){
        String[] hex = hex_str.split(":");
        byte[] returnBytes = new byte[hex.length];
        for(int i = 0; i < hex.length; i++){
            returnBytes[i] = (byte)Integer.parseInt(hex[i].substring(0), 16);
        }
        return returnBytes;
    }

    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

        @Override
        protected void initialize() {
            super.initialize();
            requestMtu(185).enqueue();
        }

        @Override
        protected boolean isRequiredServiceSupported(@NonNull BluetoothGatt gatt) {
            BluetoothDevice device = gatt.getDevice();
            String deviceName = device.getName();

            if (deviceName.startsWith("BC_PAP")) {

                BluetoothGattService mConfigService = gatt.getService(S5901);
                if (mConfigService != null) {
                    mConfigCharacteristic = mConfigService.getCharacteristic(S5902);
                }

                BluetoothGattService mDataService = gatt.getService(S5903);
                if (mDataService != null) {
                    mDataWriteCharacteristic = mDataService.getCharacteristic(S5904);
                    mDataNotifyCharacteristic = mDataService.getCharacteristic(S5905);
                }

                return mConfigService != null && mDataService != null;
            } else {
                BluetoothGattService mConfigService = gatt.getService(FF01);
                if (mConfigService != null) {
                    mConfigCharacteristic = mConfigService.getCharacteristic(_2A0D);
                }

                return mConfigService != null;
            }
        }

        @Override
        protected void onDeviceDisconnected() {
            mConfigCharacteristic = null;
            mDataWriteCharacteristic = null;
            mDataNotifyCharacteristic = null;
        }
    };
}
