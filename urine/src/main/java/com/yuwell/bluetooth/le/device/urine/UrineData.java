package com.yuwell.bluetooth.le.device.urine;

import java.util.Date;

/**
 * Created by Chen on 2015/11/12.
 */
public class UrineData {

    public String leu;
    public String bld;
    public String ph;
    public String pro;
    public String ubg;
    public String nit;
    public String vc;
    public String glu;
    public String bil;
    public String ket;
    public String sg;
    public Date time;

    public UrineData() {
        this.time = new Date();
    }

    @Override
    public String toString() {
        String str = "";
        str += "LEU：" + leu + "\n";
        str += "BLD：" + bld + "\n";
        str += "PH：" + ph + "\n";
        str += "PRO：" + pro + "\n";
        str += "UBG：" + ubg + "\n";
        str += "NIT：" + nit + "\n";
        str += "VC：" + vc + "\n";
        str += "GLU：" + glu + "\n";
        str += "BIL：" + bil + "\n";
        str += "KET：" + ket + "\n";
        str += "SG：" + sg + "\n";
        return str;
    }
}
