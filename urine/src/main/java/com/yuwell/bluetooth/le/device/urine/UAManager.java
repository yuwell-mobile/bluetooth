package com.yuwell.bluetooth.le.device.urine;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.core.YUBleManager;
import com.yuwell.bluetooth.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Urine analyzer ble manager
 * Created by Chen on 2015/9/15.
 */
public class UAManager extends YUBleManager<UAManagerCallbacks> {

    private static final byte[] DEVICE_CONFIRM = {(byte) 0x93, (byte) 0x8e, 0x08, 0x00, 0x08, 0x01, 0x43, 0x4f, 0x4e, 0x54, 0x45};
    private static final byte[] READ_SINGLE_DATA = {(byte) 0x93, (byte) 0x8e, 0x04, 0x00, 0x08, 0x04, 0x10};

    private static final byte[] MACHINE_CODE_EMP = {0x00, 0x08};

    private boolean confirm = false;
    private boolean contecDevice;

    private List<Byte> mDataReceived = new ArrayList<>();

    private BluetoothGattCharacteristic characteristicToWrite = null;
    private BluetoothGattCharacteristic notifyCharacteristic = null;

    private static UAManager managerInstance = null;

    private UAManager(Context context) {
        super(context);
    }

    public static synchronized UAManager getInstance(final Context context) {
        return new UAManager(context);
    }

    public void readLastData() {
        mDataReceived.clear();
        if (characteristicToWrite != null) {
            writeCharacteristic(characteristicToWrite, contecDevice ?
                    new byte[]{(byte) 0x91, 0x00, 0x11} : getCommand(READ_SINGLE_DATA)).enqueue();
        }
    }

    @Override
    protected BleManagerGattCallback getGattCallback() {
        return contecDevice ? mContecGattCallback : mGattCallback;
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
        String deviceName = device.getName();
        if ("BLE-EMP-Ui".equals(device.getName())) {
            contecDevice = false;
            return true;
        } else if (!TextUtils.isEmpty(deviceName) && deviceName.contains("BC01")) {
            contecDevice = true;
            return true;
        }
        return false;
    }

    private void checksum(byte[] array) {
        byte checksum = 0;
        for (int i = 2; i < array.length - 2; i++) {
            checksum += array[i];
        }
        array[array.length - 1] = (byte) (checksum & 0xff);
    }

    private byte[] getCommand(final byte[] cmd) {
        final byte[] cmdSend = new byte[cmd.length];
        System.arraycopy(cmd, 0, cmdSend, 0, cmd.length);
        cmdSend[3] = MACHINE_CODE_EMP[0];
        cmdSend[4] = MACHINE_CODE_EMP[1];
        checksum(cmdSend);
        return cmdSend;
    }

    private final BleManagerGattCallback mContecGattCallback = new BleManagerGattCallback() {

        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(notifyCharacteristic)
                .with(new DataReceivedCallback() {
                    @Override
                    public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                        byte[] value = data.getValue();

                        if (value == null) {
                            return;
                        }

                        for (byte b : value) {
                            mDataReceived.add(b);
                        }

                        if (data.size() > 13) {
                            Byte[] array = new Byte[data.size()];
                            UrineData urineData = UrineDataDecode.parse(1, mDataReceived.toArray(array));
                            mCallbacks.onUrineDataRead(device, urineData);
                        }
                    }
                });
               /* .merge(new DataMerger() {
                    @Override
                    public boolean merge(@NonNull DataStream output, @Nullable byte[] lastPacket,
                            int index) {

                        return lastPacket != null && lastPacket.length > 13;
                    }
                });*/


            enableNotifications(notifyCharacteristic).enqueue();
        }

        @Override
        protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
            BluetoothGattService mUrineService = gatt.getService(Service.URINE_CONTEC);
            if (mUrineService != null) {
                characteristicToWrite = mUrineService.getCharacteristic(Characteristic.URINE_CONTEC_WRITE);
                notifyCharacteristic = mUrineService.getCharacteristic(Characteristic.URINE_CONTEC_NOTIFY);
            }
            return mUrineService != null;
        }

        @Override
        protected void onDeviceDisconnected() {
            characteristicToWrite = null;
            notifyCharacteristic = null;
        }
    };

    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(notifyCharacteristic).with(new DataReceivedCallback() {
                @Override
                public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                    decodeData(device, data);
                }
            });

            enableNotifications(notifyCharacteristic).enqueue();
            writeCharacteristic(characteristicToWrite, setTime()).enqueue();
        }

        @Override
        protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
            BluetoothGattService mUrineService = gatt.getService(Service.URINE_MEASUREMENT);
            if (mUrineService != null) {
                characteristicToWrite = mUrineService.getCharacteristic(Characteristic.URINE_WRITE);
                notifyCharacteristic = mUrineService.getCharacteristic(Characteristic.URINE_INDICATE);
            } else if ((mUrineService = gatt.getService(Service.URINE_MEASUREMENT_NEW)) != null) {
                characteristicToWrite = mUrineService.getCharacteristic(Characteristic.URINE_NEW);
                notifyCharacteristic = characteristicToWrite;
            }
            return mUrineService != null;
        }

        @Override
        protected void onDeviceDisconnected() {
            characteristicToWrite = null;
            notifyCharacteristic = null;
        }

        private byte[] setTime() {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR) - 2000;
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int min = calendar.get(Calendar.MINUTE);

            byte[] array = new byte[12];
            array[0] = (byte) 0x93;
            array[1] = (byte) 0x8e;
            array[2] = 0x09;
            array[3] = MACHINE_CODE_EMP[0];
            array[4] = MACHINE_CODE_EMP[1];
            array[5] = 0x02;
            array[6] = (byte) Utils.decimalToHex(year);
            array[7] = (byte) Utils.decimalToHex(month);
            array[8] = (byte) Utils.decimalToHex(day);
            array[9] = (byte) Utils.decimalToHex(hour);
            array[10] = (byte) Utils.decimalToHex(min);
            checksum(array);

            return array;
        }

        private void decodeData(BluetoothDevice device, Data data) {
            byte[] value = data.getValue();

            if (value == null) {
                return;
            }

            int headerH = data.getIntValue(Data.FORMAT_UINT8, 0);
            int headerL = data.getIntValue(Data.FORMAT_UINT8, 1);

            if (headerH == 0x93 && headerL == 0x8e) {
                if (value.length > 6) {
                    switch (value[5]) {
                        case 0x01:
                            confirm = true;
                            break;
                        case 0x04:
                            mDataReceived.clear();
                            decodeWhenAllReceived(device, value);
                            break;
                    }
                }
            } else {
                decodeWhenAllReceived(device, value);
            }
        }

        private void decodeWhenAllReceived(BluetoothDevice device, byte[] value) {
            if (mDataReceived != null) {
                for (byte b : value) {
                    mDataReceived.add(b);
                }

                if (mDataReceived.size() > 2) {
                    int length = mDataReceived.get(2) & 0xff;
                    if (length + 3 == mDataReceived.size()) {
                        Byte[] array = new Byte[mDataReceived.size()];
                        UrineData urineData = UrineDataDecode.parse(0, mDataReceived.toArray(array));
                        mCallbacks.onUrineDataRead(device, urineData);
                    }
                }
            }
        }
    };
}
