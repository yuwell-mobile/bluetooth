package com.yuwell.bluetooth.le.device.urine;

import com.yuwell.bluetooth.utils.Utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Chen on 2015/11/7.
 */
class UrineDataDecode {

    private static String leu(int i) {
        return offsetMinusOne(i);
    }

    private static String nit(int i) {
        if (i == 0) {
            return "-";
        } else {
            return "+";
        }
    }

    private static String ubg(int i) {
        return offsetZero(i);
    }

    private static String pro(int i) {
        return offsetMinusOne(i);
    }

    private static String ph(int i) {
        switch (i) {
            case 0:
                return "5.0";
            case 1:
                return "6.0";
            case 2:
                return "6.5";
            case 3:
                return "7.0";
            case 4:
                return "7.5";
            case 5:
                return "8.0";
            case 6:
                return "8.5";
            default:
                return null;
        }
    }

    private static String phContec(int i) {
        return i + 5 + ".0";
    }

    private static String bld(int i) {
        return offsetMinusOne(i);
    }

    private static String sg(int i) {
        switch (i) {
            case 0:
                return "1.000";
            case 1:
                return "1.005";
            case 2:
                return "1.010";
            case 3:
                return "1.015";
            case 4:
                return "1.020";
            case 5:
                return "1.025";
            case 6:
                return "1.030";
            default:
                return null;
        }
    }

    private static String ket(int i) {
        return offsetMinusOne(i);
    }

    private static String ketContec(int i) {
        return offsetZero(i);
    }

    private static String bil(int i) {
        return offsetZero(i);
    }

    private static String glu(int i) {
        return offsetMinusOne(i);
    }

    private static String vc(int i) {
        return offsetMinusOne(i);
    }

    private static String offsetZero(int i) {
        switch (i) {
            case 0:
                return "-";
            default:
                return formatInt(i, 0);
        }
    }

    private static String offsetMinusOne(int i) {
        switch (i) {
            case 0:
                return "-";
            case 1:
                return "+-";
            default:
                return formatInt(i, -1);
        }
    }

    private static String formatInt(int i, int offset) {
        return "+" + (i + offset);
    }

    static UrineData parse(int type, Byte[] data) {
        switch (type) {
            case 0:
                return empDecode(data);
            case 1:
                return contecDecode(data);
            default:
                return null;
        }
    }

    /**
     * 恩普数据解析
     * @param data
     */
    private static UrineData empDecode(Byte[] data) {
        UrineData urineData = new UrineData();

        int ymd = (Utils.unsignedByteToInt(data[10]) << 8) + Utils.unsignedByteToInt(data[11]);
        int lmh = (Utils.unsignedByteToInt(data[12]) << 8) + Utils.unsignedByteToInt(data[13]);

        urineData.time = getDate(ymd & 0x7f, ymd >> 7 & 0xf,
                ymd >> 11 & 0x1f, lmh & 0x1f,
                lmh >> 5 & 0x3f, 0);

        urineData.leu = UrineDataDecode.leu(lmh >> 11 & 0x7);

        int bppun = (Utils.unsignedByteToInt(data[14]) << 8) + Utils.unsignedByteToInt(
                data[15]);
        urineData.nit = UrineDataDecode.nit(bppun & 0x7);
        urineData.ubg = UrineDataDecode.ubg(bppun >> 3 & 0x7);
        urineData.pro = UrineDataDecode.pro(bppun >> 6 & 0x7);
        urineData.ph = UrineDataDecode.ph(bppun >> 9 & 0x7);
        urineData.bld = UrineDataDecode.bld(bppun >> 12 & 0x7);

        int vgbks = (Utils.unsignedByteToInt(data[16]) << 8) + Utils.unsignedByteToInt(
                data[17]);
        urineData.vc = UrineDataDecode.vc(vgbks >> 12 & 0x7);
        urineData.glu = UrineDataDecode.glu(vgbks >> 9 & 0x7);
        urineData.bil = UrineDataDecode.bil(vgbks >> 6 & 0x7);
        urineData.ket = UrineDataDecode.ket(vgbks >> 3 & 0x7);
        urineData.sg = UrineDataDecode.sg(vgbks & 0x7);

        return urineData;
    }

    /**
     * 康泰数据解析
     * @param data
     */
    private static UrineData contecDecode(Byte[] data) {
        UrineData urineData = new UrineData();

        urineData.time = getDate(data[2] & 0x7f, data[3] & 0xf, data[4] & 0x1f,
                data[5] & 0x1f, data[6] & 0x3f, data[7] & 0x3f);

        urineData.ubg = UrineDataDecode.ubg(data[8] & 0x7);
        urineData.bld = UrineDataDecode.bld(data[8] >> 3 & 0x7);
        urineData.bil = UrineDataDecode.bil(data[9] & 0x7);
        urineData.ket = UrineDataDecode.ketContec(data[9] >> 3 & 0x7);
        urineData.glu = UrineDataDecode.glu(data[10] & 0x7);
        urineData.pro = UrineDataDecode.pro(data[10] >> 3 & 0x7);
        urineData.ph = UrineDataDecode.phContec(data[11] & 0x7);
        urineData.nit = UrineDataDecode.nit(data[11] >> 3 & 0x7);
        urineData.leu = UrineDataDecode.leu(data[12] & 0x7);
        urineData.sg = UrineDataDecode.sg((data[12] >> 3 & 0x7) + 1);
        urineData.vc = UrineDataDecode.vc(data[13] & 0x7);

        return urineData;
    }

    private static Date getDate(int year, int month, int day, int hour, int min, int sec) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year + 2000);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, sec);
        return calendar.getTime();
    }
}
