package com.yuwell.bluetooth.le.device.urine;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2016/6/27.
 */
public interface UAManagerCallbacks extends BleManagerCallbacks {

    void onUrineDataRead(@NonNull final BluetoothDevice device, @NonNull UrineData data);
}
