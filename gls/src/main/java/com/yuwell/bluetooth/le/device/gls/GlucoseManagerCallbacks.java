package com.yuwell.bluetooth.le.device.gls;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import com.yuwell.bluetooth.le.device.battery.BatteryManagerCallbacks;

import java.util.List;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2016/6/27.
 */
public interface GlucoseManagerCallbacks extends BleManagerCallbacks, BatteryManagerCallbacks {

    void onGlucoseMeasurementRead(@NonNull final BluetoothDevice device, @NonNull final GlucoseData data);

    void onGlucoseErrorRead(@NonNull final BluetoothDevice device, int errorCode);

    void onBloodTook();

    void onHistoryGlucoseMeasurementsRead(@NonNull final BluetoothDevice device, List<GlucoseData> data);
}
