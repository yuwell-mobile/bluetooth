package com.yuwell.bluetooth.le.device.gls;

import android.bluetooth.BluetoothDevice;

import androidx.annotation.NonNull;

import no.nordicsemi.android.ble.common.callback.glucose.GlucoseMeasurementDataCallback;
import no.nordicsemi.android.ble.data.Data;

/**
 * Created by Chen on 2020/12/8.
 */
public abstract class YuwellGlucoseMeasurementDataCallback extends GlucoseMeasurementDataCallback {

    @Override
    public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
        if (data.size() < 10) {
            onInvalidDataReceived(device, data);
            return;
        }

        int offset = 0;

        final int flags = data.getIntValue(Data.FORMAT_UINT8, offset++);
        final boolean timeOffsetPresent = (flags & 0x01) != 0;
        final boolean glucoseDataPresent = (flags & 0x02) != 0;
        final boolean unitMolL = (flags & 0x04) != 0;
        final boolean sensorStatusAnnunciationPresent = (flags & 0x08) != 0;
        final boolean contextInformationFollows = (flags & 0x10) != 0;
        final boolean errorPresent = (flags & 0x20) != 0;

        if (data.size() < 10 + (timeOffsetPresent ? 2 : 0) + (glucoseDataPresent ? 3 : 0)
                + (sensorStatusAnnunciationPresent ? 2 : 0)) {
            onInvalidDataReceived(device, data);
            return;
        }

        if (errorPresent) {
            onErrorReceived(data.getIntValue(Data.FORMAT_UINT16, offset));
        } else {
            super.onDataReceived(device, data);
        }
    }

    public abstract void onErrorReceived(int errorCode);
}
