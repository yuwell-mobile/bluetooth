package com.yuwell.bluetooth.le.device.gls;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.device.battery.BatteryManager;
import com.yuwell.bluetooth.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.common.callback.RecordAccessControlPointDataCallback;
import no.nordicsemi.android.ble.common.callback.glucose.GlucoseMeasurementDataCallback;
import no.nordicsemi.android.ble.common.data.RecordAccessControlPointData;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.ble.data.MutableData;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Glucometer behavior
 * Created by Chen on 2015/9/9.
 */
public class GlucoseManager extends BatteryManager<GlucoseManagerCallbacks> {

	private static final String TAG = GlucoseManager.class.getSimpleName();

	public static final String YUWELL_GLUCOSE = "Yuwell Glucose";

	private BluetoothGattCharacteristic mGMCharacteristic;
	private BluetoothGattCharacteristic mGlucoseContext;
	private BluetoothGattCharacteristic mDateTimeCharacteristic;
	private BluetoothGattCharacteristic mCurrentTimeCharacteristic;
	private BluetoothGattCharacteristic mRecordAccessControlPointCharacteristic;

	private static final int FILTER_TYPE_USER_FACING_TIME = 2;

	private final static int OP_CODE_REPORT_STORED_RECORDS = 1;
	private final static int OP_CODE_DELETE_STORED_RECORDS = 2;
	private final static int OP_CODE_ABORT_OPERATION = 3;
	private final static int OP_CODE_REPORT_NUMBER_OF_RECORDS = 4;
	private final static int OP_CODE_NUMBER_OF_STORED_RECORDS_RESPONSE = 5;
	private final static int OP_CODE_RESPONSE_CODE = 6;

	private final static int OPERATOR_NULL = 0;
	private final static int OPERATOR_ALL_RECORDS = 1;
	private final static int OPERATOR_LESS_THEN_OR_EQUAL = 2;
	private final static int OPERATOR_GREATER_THEN_OR_EQUAL = 3;
	private final static int OPERATOR_WITHING_RANGE = 4;
	private final static int OPERATOR_FIRST_RECORD = 5;
	private final static int OPERATOR_LAST_RECORD = 6;

	private List<GlucoseData> historyList = new ArrayList<>();
	private boolean readingRacp = false;
	private boolean removeFirst = false;

	private static GlucoseManager managerInstance = null;

	private Handler mHandler = new Handler();

	private boolean clearCache = false;

	public GlucoseManager(Context context) {
		super(context);
	}

	/**
	 * Returns the singleton implementation of BPMManager
	 */
	public static synchronized GlucoseManager getInstance(final Context context) {
		return new GlucoseManager(context);
	}

	public static boolean isSpecfiedDevice(String deviceName) {
		return YUWELL_GLUCOSE.equals(deviceName) || (!TextUtils.isEmpty(deviceName) && deviceName.contains("Yuwell BG"));
	}

	@Override
	protected BleManagerGattCallback getGattCallback() {
		return mGattCallback;
	}

	@SuppressLint("MissingPermission")
	@Override
	public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
		return super.connectable(device, rssi, scanRecord) && isSpecfiedDevice(device.getName());
	}

	@Override
	protected boolean shouldClearCacheWhenDisconnected() {
		return clearCache;
	}

	public void setClearCache(boolean clearCache) {
		this.clearCache = clearCache;
	}

	public void getAllRecords(boolean removeFirst) {
		if (mRecordAccessControlPointCharacteristic == null) return;

		this.removeFirst = removeFirst;
		historyList.clear();

		writeCharacteristic(mRecordAccessControlPointCharacteristic, RecordAccessControlPointData.reportAllStoredRecords())
				.done(device -> readingRacp = true)
				.enqueue();
	}

	/** Sends the request to delete all data from the device. */
	public void deleteAllRecords() {
		if (mRecordAccessControlPointCharacteristic == null) return;

		writeCharacteristic(mRecordAccessControlPointCharacteristic, RecordAccessControlPointData.deleteAllStoredRecords()).enqueue();
	}

	public void getSpecificRecord(Date date, boolean greater) {
		if (mRecordAccessControlPointCharacteristic == null) return;

		historyList.clear();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);

		int[] array = new int[7];
		array[0] = year & 0xff;
		array[1] = (year >> 8) & 0xFF;
		array[2] = month & 0xff;
		array[3] = day & 0xff;
		array[4] = hour & 0xff;
		array[5] = min & 0xff;
		array[6] = 0;

		final BluetoothGattCharacteristic characteristic = mRecordAccessControlPointCharacteristic;
		Data data = setOpCode(OP_CODE_REPORT_STORED_RECORDS,
				greater ? OPERATOR_GREATER_THEN_OR_EQUAL : OPERATOR_LESS_THEN_OR_EQUAL,
				array);

		writeCharacteristic(characteristic, data)
				.done(device -> {
					readingRacp = true;
				}).enqueue();
	}

	private void onHistoryReadFinish(BluetoothDevice device) {
		if (readingRacp && historyList != null && historyList.size() > 0) {
			if (removeFirst) {
				historyList.remove(0);
			}

			mCallbacks.onHistoryGlucoseMeasurementsRead(device, new ArrayList<>(historyList));
			readingRacp = false;
		}
	}

	private Data setOpCode(final int opCode, final int operator, final int... params) {
		return setOpCode(opCode, operator, FILTER_TYPE_USER_FACING_TIME, params);
	}

	private Data setOpCode(final int opCode, final int operator,
						   final int filterType, final int... params) {
		final int size = 2 + ((params.length > 0) ? 1 : 0) + params.length;

		MutableData data = new MutableData(new byte[size]);
		// write the operation code
		int offset = 0;
		data.setValue(opCode, Data.FORMAT_UINT8, offset);
		offset += 1;

		// write the operator. This is always present but may be equal to OPERATOR_NULL
		data.setValue(operator, Data.FORMAT_UINT8, offset);
		offset += 1;

		// if parameters exists, append them. Parameters should be sorted from minimum to maximum.
		// Currently only one or two params are allowed
		if (params.length > 0) {
			data.setValue(filterType, Data.FORMAT_UINT8, offset);
			offset += 1;

			for (final Integer i : params) {
				data.setValue(i, Data.FORMAT_UINT8, offset);
				offset += 1;
			}
		}

		return data;
	}

	private final BleManagerGattCallback mGattCallback = new BatteryManagerGattCallback() {

		@Override
		protected void initialize() {
			super.initialize();
			setNotificationCallback(mGMCharacteristic)
					.with(new GlucoseMeasurementDataCallback() {
						@Override
						public void onGlucoseMeasurementReceived(@NonNull BluetoothDevice device,
								int sequenceNumber, @NonNull Calendar time,
								@Nullable Float glucoseConcentration, @Nullable Integer unit,
								@Nullable Integer type, @Nullable Integer sampleLocation,
								@Nullable GlucoseStatus status,
								boolean contextInformationFollows) {

							GlucoseData glucoseData = new GlucoseData();

							if (glucoseConcentration != null) {
								glucoseData.value = glucoseConcentration;
							}

							if (unit != null) {
								glucoseData.unit = unit;
								if (unit == GlucoseData.UNIT_molpl && glucoseConcentration != null) {
									glucoseData.value = Utils.multiply(glucoseConcentration, 1000);
								}
							}

							glucoseData.time = time.getTime();
							glucoseData.type = GlucoseData.TYPE_GLU;
							mCallbacks.onGlucoseMeasurementRead(device, glucoseData);
						}
					});
			setNotificationCallback(mGlucoseContext).with(new DataReceivedCallback() {
				@Override
				public void onDataReceived(@androidx.annotation.NonNull BluetoothDevice device, @androidx.annotation.NonNull Data data) {
					int flag = data.getIntValue(Data.FORMAT_UINT8, 0);
					if (flag == 0x0e) {
						int value = data.getIntValue(Data.FORMAT_UINT16, 1);
						if (value == 0x7ff) {
							Log.d(TAG, "onDataReceived: blood took");
							mCallbacks.onBloodTook();
						} else {
							/*
							错误码类型				错误码	屏幕显示
							1、血糖反应区被污染		0x0001	E01
							2、code码异常			0x0002	E02
							3、进样超时				0x0003	E05
							4、质控液测量电流有跳值	0x0004	不确定
							5、阻抗值超出范围			0x0005	不确定
							6、HCT值超出范围			0x0006	不确定
							7、进样检测区被污染		0x0007	不确定
							8、阻抗反应区被污染		0x0008	不确定
							9、血糖测量激励电压异常	0x0009	不确定
							10、HCT测量激励电压异常	0x000A	不确定
							11、code引脚短路			0x000B	不确定
							12、未校准				0x000C	仪器未校准
							13、低电量				0x000D	显示电池图标	+LO
							14、温度过高				0x000E	显示温度图标	+HI
							15、温度过低				0x000F	显示温度图标	+LO
							 */
							Log.d(TAG, "onDataReceived: error" + value);
							mCallbacks.onGlucoseErrorRead(device, value);
						}
					}
				}
			});
			enableNotifications(mGMCharacteristic).enqueue();
			enableNotifications(mGlucoseContext).enqueue();
			writeCharacteristic(getTimeWriteCharacteristic(), setDateTime())
					.done(device -> mHandler.postDelayed(() -> {readBatteryLevelCharacteristic();}, 2000))
					.enqueue();

			if (mRecordAccessControlPointCharacteristic != null) {
				setIndicationCallback(mRecordAccessControlPointCharacteristic).with(new RecordAccessControlPointDataCallback() {
					@Override
					public void onRecordAccessOperationCompleted(@androidx.annotation.NonNull BluetoothDevice device, int requestCode) {
						Log.d(TAG, "onRecordAccessOperationCompleted: " + requestCode);
						if (requestCode == OP_CODE_REPORT_STORED_RECORDS) {
							onHistoryReadFinish(device);
						}
					}

					@Override
					public void onRecordAccessOperationCompletedWithNoRecordsFound(@androidx.annotation.NonNull BluetoothDevice device, int requestCode) {
						Log.d(TAG, "onRecordAccessOperationCompletedWithNoRecordsFound: " + requestCode);
					}

					@Override
					public void onNumberOfRecordsReceived(@androidx.annotation.NonNull BluetoothDevice device, int numberOfRecords) {
						Log.d(TAG, "onNumberOfRecordsReceived: " + numberOfRecords);
					}

					@Override
					public void onRecordAccessOperationError(@androidx.annotation.NonNull BluetoothDevice device, int requestCode, int errorCode) {
						Log.d(TAG, "onRecordAccessOperationError:" + requestCode + ", error:" + errorCode);
					}
				});
				enableIndications(mRecordAccessControlPointCharacteristic).enqueue();
			}
		}

		@Override
		protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
			BluetoothGattService mBGService = gatt.getService(Service.BLOOD_GLUCOSE_MEASUREMMENT);
			if (mBGService != null) {
				mDateTimeCharacteristic = mBGService.getCharacteristic(Characteristic.DATE_TIME);
				mGMCharacteristic = mBGService.getCharacteristic(Characteristic.BLOOD_GLUCOSE);
				mGlucoseContext = mBGService.getCharacteristic(Characteristic.GLUCOSE_MEASUREMENT_CONTEXT);
				mRecordAccessControlPointCharacteristic = mBGService.getCharacteristic(Characteristic.RACP);
			}

			BluetoothGattService mCurrentTime = gatt.getService(Service.CURRENT_TIME);
			if (mCurrentTime != null) {
				mCurrentTimeCharacteristic = mCurrentTime.getCharacteristic(Characteristic.CURRENT_TIME);
			}

			return mBGService != null;
		}

		@Override
		protected void onDeviceDisconnected() {
			super.onDeviceDisconnected();
			mCurrentTimeCharacteristic = null;
			mDateTimeCharacteristic = null;
			mGMCharacteristic = null;
			mRecordAccessControlPointCharacteristic = null;
			mGlucoseContext = null;
		}

		private BluetoothGattCharacteristic getTimeWriteCharacteristic() {
			if (mCurrentTimeCharacteristic != null) {
				return mCurrentTimeCharacteristic;
			} else if (mDateTimeCharacteristic != null) {
				return mDateTimeCharacteristic;
			}
			return null;
		}

		/**
		 * 设置时间
		 */
		private byte[] setDateTime() {
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			int hour = calendar.get(Calendar.HOUR_OF_DAY);
			int min = calendar.get(Calendar.MINUTE);
			int sec = calendar.get(Calendar.SECOND);

			int byteSize = 0;
			if (mDateTimeCharacteristic != null) {
				byteSize = 7;
			}
			if (mCurrentTimeCharacteristic != null) {
				byteSize = 10;
			}

			byte[] array = new byte[byteSize];
			array[0] = (byte) (year & 0xFF);
			array[1] = (byte) ((year >> 8) & 0xFF);
			array[2] = (byte) (month & 0xFF);
			array[3] = (byte) (day & 0xFF);
			array[4] = (byte) (hour & 0xFF);
			array[5] = (byte) (min & 0xFF);
			array[6] = (byte) (sec & 0xFF);

			return array;
		}
	};
}

