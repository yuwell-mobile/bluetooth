package com.yuwell.bluetooth.le.device.hts;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.core.YUBleManager;
import com.yuwell.bluetooth.utils.Utils;

import java.util.Calendar;

import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.common.callback.ht.TemperatureMeasurementDataCallback;
import no.nordicsemi.android.ble.common.profile.ht.TemperatureMeasurementCallback;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;


/**
 * Thermometer device
 * Created by Chen on 2015/11/30.
 */
public class ThermometerManager extends YUBleManager<HTSManagerCallbacks> {

    private static final String TAG = "ThermometerManager";

    private static final String TAIDOC = "TAIDOC";

    private BluetoothGattCharacteristic mMeasurement;
    private BluetoothGattCharacteristic mHTMeasurement;
    private BluetoothGattCharacteristic mRXCharacteristic, mTXCharacteristic;

    private long last;

    private int type;

    private static ThermometerManager managerInstance = null;

    private ThermometerManager(Context context) {
        super(context);
    }

    public static synchronized ThermometerManager getInstance(final Context context) {
        return new ThermometerManager(context);
    }

    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
        String deviceName = device.getName();
        if (super.connectable(device, rssi, scanRecord)) {
            if ("MEDXING-IRT".equalsIgnoreCase(device.getName())) {
                return true;
            } else if ((!TextUtils.isEmpty(deviceName) && deviceName.contains(TAIDOC))) {
                return true;
            } else if (!TextUtils.isEmpty(deviceName) && deviceName.contains("Radiant")) {
                return true;
            } else if (!TextUtils.isEmpty(deviceName) && deviceName.contains("Yuwell HT")) {
                return true;
            }
        }
        return false;
    }

    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(mHTMeasurement).with(new TemperatureMeasurementDataCallback() {

                @Override
                public void onTemperatureMeasurementReceived(@NonNull BluetoothDevice device,
                        float temperature, int unit,
                        @Nullable Calendar calendar,
                        @Nullable Integer type) {

                    String strVal = Utils.retainOneDecimal(String.valueOf(temperature));
                    mCallbacks.onHTValueReceived(device, Utils.roundHalfUpWithScale1(Double.parseDouble(strVal)), unit);
                }
            });

            setNotificationCallback(mMeasurement).with(new DataReceivedCallback() {
                @Override
                public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                    byte[] bytes = data.getValue();
                    if (bytes != null && bytes.length >= 10 &&
                            bytes[0] == (byte) 0xff && bytes[1] == (byte) 0xfe && bytes[5] == (byte) 0x65) {

                        long current = System.currentTimeMillis();
                        if (current - last < 500) {
                            Log.d(TAG, "Received 2nd time");
                            return;
                        }

                        last = current;

                        double temperature = (Utils.unsignedByteToInt(bytes[6]) +
                                Utils.unsignedByteToInt(bytes[7]) * 256) / 10f;
                        mCallbacks.onHTValueReceived(device, Utils.roundHalfUpWithScale1(temperature), TemperatureMeasurementCallback.UNIT_C);
                    }
                }
            });

            setNotificationCallback(mTXCharacteristic).with(new DataReceivedCallback() {
                @Override
                public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                    try {
                        String val = data.getStringValue(0).replace("*C", "");
                        mCallbacks.onHTValueReceived(device, Double.valueOf(val), TemperatureMeasurementCallback.UNIT_C);
                    } catch (Exception e) {
                        Log.e(TAG, "onDataReceived: ", e);
                    }
                }
            });

            if (mMeasurement != null) {
                enableNotifications(mMeasurement).enqueue();
            }
            if (mHTMeasurement != null) {
                enableIndications(mHTMeasurement).enqueue();
            }
            if (mTXCharacteristic != null) {
                enableNotifications(mTXCharacteristic).enqueue();
            }
        }

        @Override
        protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
            BluetoothGattService mService = gatt.getService(Service.THERMOMETER);
            BluetoothGattService mHTService = gatt.getService(Service.HEALTH_THERMOMETER);
            if (mService != null) {
                mMeasurement = mService.getCharacteristic(Characteristic.THERMOMETER);
                return true;
            }
            if (mHTService != null) {
                mHTMeasurement = mHTService.getCharacteristic(Characteristic.HT_MEASUREMENT);
                return true;
            }

            BluetoothGattService service = gatt.getService(Service.UART_SERVICE_UUID);
            if (service != null) {
                mRXCharacteristic = service.getCharacteristic(Characteristic.UART_RX_CHARACTERISTIC_UUID);
                mTXCharacteristic = service.getCharacteristic(Characteristic.UART_TX_CHARACTERISTIC_UUID);
                return mRXCharacteristic != null && mTXCharacteristic != null;
            }

            return false;
        }

        @Override
        protected void onDeviceDisconnected() {
            mMeasurement = null;
            mHTMeasurement = null;
            mTXCharacteristic = null;
            mRXCharacteristic = null;
        }

    };
}
