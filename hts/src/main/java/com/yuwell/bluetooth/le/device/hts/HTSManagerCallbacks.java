package com.yuwell.bluetooth.le.device.hts;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2016/6/27.
 */
public interface HTSManagerCallbacks extends BleManagerCallbacks {

    void onHTValueReceived(@NonNull final BluetoothDevice device, double value, int unit);
}
