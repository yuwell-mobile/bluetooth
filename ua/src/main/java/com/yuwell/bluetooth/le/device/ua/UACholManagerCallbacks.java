package com.yuwell.bluetooth.le.device.ua;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2016/6/27.
 */
public interface UACholManagerCallbacks extends BleManagerCallbacks {

    void onUADataRead(@NonNull final BluetoothDevice device, @NonNull UACholData data);
}
