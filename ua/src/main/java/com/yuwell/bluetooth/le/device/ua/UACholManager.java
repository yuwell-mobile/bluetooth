package com.yuwell.bluetooth.le.device.ua;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.core.YUBleManager;
import com.yuwell.bluetooth.utils.Utils;

import java.util.Calendar;

import no.nordicsemi.android.ble.common.callback.glucose.GlucoseMeasurementDataCallback;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * BeneCheck behavior
 * Created by Chen on 2019/6/11.
 */
public class UACholManager extends YUBleManager<UACholManagerCallbacks> {


	private BluetoothGattCharacteristic mGMCharacteristic;

	private static UACholManager managerInstance = null;
	private long lastTime;

	protected UACholManager(Context context) {
		super(context);
	}

	/**
	 * Returns the singleton implementation of BPMManager
	 */
	public static synchronized UACholManager getInstance(final Context context) {
		return new UACholManager(context);
	}

	public static boolean isSpecfiedDevice(String deviceName) {
		return !TextUtils.isEmpty(deviceName) && deviceName.contains("BeneCheck");
	}

	@Override
	protected BleManagerGattCallback getGattCallback() {
		return mGattCallback;
	}

	@SuppressLint("MissingPermission")
	@Override
	public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
		return super.connectable(device, rssi, scanRecord) && isSpecfiedDevice(device.getName());
	}

	private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

		@Override
		protected void initialize() {
			super.initialize();
			setNotificationCallback(mGMCharacteristic).with(new GlucoseMeasurementDataCallback() {
				@Override
				public void onGlucoseMeasurementReceived(@NonNull BluetoothDevice device,
						int sequenceNumber, @NonNull Calendar time,
						@Nullable Float glucoseConcentration, @Nullable Integer unit,
						@Nullable Integer type, @Nullable Integer sampleLocation,
						@Nullable GlucoseStatus status, boolean contextInformationFollows) {

					if (lastTime == time.getTimeInMillis()) {
						return;
					} else {
						lastTime = time.getTimeInMillis();
					}

					int scale = 0;
					UACholData glucoseData = new UACholData();
					glucoseData.time = time.getTime();

					switch (sequenceNumber) {
						case 0x41:
							glucoseData.type = UACholData.TYPE_GLU;
							scale = 1;
							break;
						case 0x51:
							glucoseData.type = UACholData.TYPE_UA;
							scale = 3;
							break;
						case 0x61:
							glucoseData.type = UACholData.TYPE_CHOL;
							scale = 2;
							break;
					}

					if (glucoseConcentration != null && unit != null) {
						if (unit == UACholData.UNIT_kgpl) {
							glucoseData.value = glucoseConcentration * 100000;
						} else {
							glucoseData.value = Utils.multiply(glucoseConcentration, 1000, scale);
						}
						glucoseData.unit = unit;
					}

					mCallbacks.onUADataRead(device, glucoseData);
				}
			});

			enableNotifications(mGMCharacteristic).enqueue();
		}

		@Override
		protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
			BluetoothGattService mBGService = gatt.getService(Service.BLOOD_GLUCOSE_MEASUREMMENT);
			if (mBGService != null) {
				mGMCharacteristic = mBGService.getCharacteristic(Characteristic.BLOOD_GLUCOSE);
			}

			return mBGService != null;
		}

		@Override
		protected void onDeviceDisconnected() {
			mGMCharacteristic = null;
		}
	};
}

