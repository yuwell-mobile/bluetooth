package com.yuwell.bluetooth.le.device.ua;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chen on 2018/5/31.
 */
public class UACholData {

	public static final int TYPE_GLU = 0;
	public static final int TYPE_UA = 1;
	public static final int TYPE_CHOL = 2;

	public static final int UNIT_kgpl = 0;
	public static final int UNIT_molpl = 1;

	public int type;
	public float value;
	public Date time; // Device time, may not correct
	public int unit; // 0

	public String toJsonString() {
		Map<String, Object> map = new HashMap<>();
		map.put("value", value);
		map.put("time", time.getTime());
		map.put("type", type);
		map.put("unit", unit);

		return new JSONObject(map).toString();
	}
}
