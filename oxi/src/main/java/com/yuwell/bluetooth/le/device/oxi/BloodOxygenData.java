package com.yuwell.bluetooth.le.device.oxi;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chen on 2018/5/31.
 */
public class BloodOxygenData {

	public int pulseRate;
	public int saturation;
	public float pi;

	public String toJsonString() {
		Map<String, Object> map = new HashMap<>();
		map.put("pulseRate", pulseRate);
		map.put("saturation", saturation);
		map.put("pi", pi);

		return new JSONObject(map).toString();
	}
}
