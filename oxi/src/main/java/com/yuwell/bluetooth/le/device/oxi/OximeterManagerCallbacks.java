package com.yuwell.bluetooth.le.device.oxi;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2016/6/27.
 */
public interface OximeterManagerCallbacks extends BleManagerCallbacks {

    void onOxygenDataRead(@NonNull final BluetoothDevice device, @NonNull BloodOxygenData data);

    void onPulseWaveRead(@NonNull final BluetoothDevice device, int pulseWave);

    void onSensorOff(@NonNull final BluetoothDevice device);
}
