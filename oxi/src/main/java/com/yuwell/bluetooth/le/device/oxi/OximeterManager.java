package com.yuwell.bluetooth.le.device.oxi;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.contec.spo2.code.base.ContecDevice;
import com.contec.spo2.code.callback.ConnectCallback;
import com.contec.spo2.code.callback.RealtimeSpO2Callback;
import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.core.YUBleManager;
import com.yuwell.bluetooth.utils.Utils;

import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Oximeter ble manager
 * Created by Chen on 2015/9/14.
 */
public class OximeterManager extends YUBleManager<OximeterManagerCallbacks> {

    public static final String TAG = OximeterManager.class.getSimpleName();

    private static OximeterManager managerInstance = null;

    private BluetoothGattCharacteristic mMeasurement;

    private BluetoothGattCharacteristic mCharacteristicWrite;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private ContecDevice contecDevice;

    private Handler mHandler = new Handler();

    private boolean isContecDevice;

    private OximeterManager(Context context) {
        super(context);

        try {
            Class clazz = Class.forName("com.contec.spo2.code.a.g");
            contecDevice = (ContecDevice) clazz.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static synchronized OximeterManager getInstance(final Context context) {
        return new OximeterManager(context);
    }

    public static boolean isSpecfiedDevice(String deviceName) {
        if (TextUtils.isEmpty(deviceName)) {
            return false;
        }

        return deviceName.contains("Tv2") || deviceName.contains("DualSpp")
                || deviceName.contains("Yuwell Oxi") || deviceName.startsWith("SpO208")
                || deviceName.contains("Yuwell BO");
    }

    @Override
    protected BleManagerGattCallback getGattCallback() {
        return isContecDevice ? mContecGattCallback : mGattCallback;
    }

    @Override
    @SuppressLint("MissingPermission")
    public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
        String name = device.getName();
        if (!TextUtils.isEmpty(name)) {
            isContecDevice = name.startsWith("SpO208");
            return super.connectable(device, rssi, scanRecord) && isSpecfiedDevice(name);
        } else {
            return false;
        }
    }

    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(mMeasurement).with(new DataReceivedCallback() {
                @Override
                public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                    decodeData(device, data);
                }
            });

            enableNotifications(mMeasurement).enqueue();
        }

        @Override
        protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
            BluetoothGattService mOxiService = gatt.getService(Service.OXIMETER);
            if (mOxiService != null) {
                mMeasurement = mOxiService.getCharacteristic(Characteristic.OXIMETER);
            }
            return mOxiService != null;
        }

        @Override
        protected void onDeviceDisconnected() {
            mMeasurement = null;
        }

        private void handlePackage56(BluetoothDevice device, Data data) {
            Integer waveHead = data.getIntValue(Data.FORMAT_UINT8, 0);
            Integer packageId = data.getIntValue(Data.FORMAT_UINT8, 2);

            if (waveHead != null && waveHead == 0xFE &&
                    packageId != null && packageId == 0x56) {
                Integer pulseWave = data.getIntValue(Data.FORMAT_UINT8, 3);
                Integer warning = data.getIntValue(Data.FORMAT_UINT8, 4);

                mCallbacks.onPulseWaveRead(device, pulseWave);
                if (((warning & 0x2) >> 1) == 1) {
                    mCallbacks.onSensorOff(device);
                }
            }
        }

        private void handlePackage55(BluetoothDevice device, Data data, byte[] value, int startOffset) {
            Integer waveHead = data.getIntValue(Data.FORMAT_UINT8, startOffset);
            Integer packageId = data.getIntValue(Data.FORMAT_UINT8, startOffset + 2);

            if (waveHead != null && waveHead == 0xFE
                    && packageId != null && packageId == 0x55) {

                BloodOxygenData bod = new BloodOxygenData();
                int pr = (Utils.unsignedByteToInt(value[startOffset + 3]) << 8) + Utils.unsignedByteToInt(value[startOffset + 4]);
                Integer spo2 = data.getIntValue(Data.FORMAT_UINT8, startOffset + 5);
                float pi = ((Utils.unsignedByteToInt(value[startOffset + 6]) << 8) + Utils.unsignedByteToInt(value[startOffset + 7])) / 1000f;

                if (spo2 != null && spo2 < 101) {
                    bod.pulseRate = pr;
                    bod.saturation = spo2;
                    bod.pi = pi;
                    mCallbacks.onOxygenDataRead(device, bod);
                }
            }
        }

        private void decodeData(BluetoothDevice device, Data data) {
            byte[] value = data.getValue();

            if (value == null) {
                return;
            }

            if (value.length == 8) {
                // (0x) FE-08-56
                handlePackage56(device, data);
            } else if (value.length == 10) {
                handlePackage55(device, data, value, 0);
            } else if (value.length >= 16) {
                handlePackage56(device, data);
                handlePackage55(device, data, value, 8);
            }
        }
    };

    /**
     * Impl of Contec oximeter gatt callback
     */
    private final BleManagerGattCallback mContecGattCallback = new BleManagerGattCallback() {

        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(mNotifyCharacteristic).with(new DataReceivedCallback() {
                @Override
                public void onDataReceived(@androidx.annotation.NonNull BluetoothDevice device, @androidx.annotation.NonNull Data data) {
                    decodeData(device, data);
                }
            });

            enableNotifications(mNotifyCharacteristic).enqueue();

            contecDevice.setConnectStatus(true);
            contecDevice.setCommunicationManager(communicationManager);
            contecDevice.startRealtimeSpO2(new RealtimeSpO2Callback() {
                @Override
                public void onRealtimeSpo2Data(final int spo2, final int pr, final int pi) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            BloodOxygenData bod = new BloodOxygenData();
                            bod.pi = pi;
                            bod.pulseRate = pr;
                            bod.saturation = spo2;
                            mCallbacks.onOxygenDataRead(getBluetoothDevice(), bod);
                        }
                    });
                }

                @Override
                public void onRealtimeSpo2End() {
                    Log.d(TAG, "onRealtimeSpo2End");
                }

                @Override
                public void onFail(int i) {
                    Log.d(TAG, "onFail");
                }
            });
        }

        @Override
        protected boolean isRequiredServiceSupported(@androidx.annotation.NonNull BluetoothGatt gatt) {
            BluetoothGattService mOxiService = gatt.getService(Service.URINE_CONTEC);
            if (mOxiService != null) {
                mCharacteristicWrite = mOxiService.getCharacteristic(Characteristic.URINE_CONTEC_WRITE);
                mNotifyCharacteristic = mOxiService.getCharacteristic(Characteristic.URINE_CONTEC_NOTIFY);
            }
            return mOxiService != null;
        }

        @Override
        protected void onDeviceDisconnected() {
            mCharacteristicWrite = mNotifyCharacteristic = null;
        }

        private void decodeData(BluetoothDevice device, Data data) {
            if (contecDevice != null) {
                contecDevice.onDataReceived(data.getValue());
            }
        }
    };

    private com.contec.spo2.code.base.a communicationManager = new com.contec.spo2.code.base.a() {
        @Override
        public void a(ConnectCallback connectCallback) {

        }

        @Override
        public void a() {

        }

        @Override
        public void a(byte[] bytes) {
            if (isConnected()) {
                writeCharacteristic(mCharacteristicWrite, bytes).enqueue();
            }
        }
    };
}
