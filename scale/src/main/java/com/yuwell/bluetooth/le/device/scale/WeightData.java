package com.yuwell.bluetooth.le.device.scale;

import java.text.DecimalFormat;

/**
 * Created by Chen on 2016/5/13.
 */
public class WeightData {

    private static DecimalFormat dfc = new DecimalFormat("#.#");

    private String type;
    private String level;
    // 1 男
    private int sex;
    private int age;
    private int height;
    private String weight;
    private String fat;
    private String bones;
    private String muscle;
    private String visceralFat;
    private String waterRate;
    private String bmr;
    private int bodyAge;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBones() {
        return bones;
    }

    public void setBones(String bones) {
        this.bones = bones;
    }

    public String getMuscle() {
        return muscle;
    }

    public void setMuscle(String muscle) {
        this.muscle = muscle;
    }

    public String getVisceralFat() {
        return visceralFat;
    }

    public void setVisceralFat(String visceralFat) {
        this.visceralFat = visceralFat;
    }

    public String getWaterRate() {
        return waterRate;
    }

    public void setWaterRate(String waterRate) {
        this.waterRate = waterRate;
    }

    public String getBmr() {
        return bmr;
    }

    public void setBmr(String bmr) {
        this.bmr = bmr;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public int getBodyAge() {
        return bodyAge;
    }

    public void setBodyAge(int bodyAge) {
        this.bodyAge = bodyAge;
    }

    public String getBmi() {
        if (height == 0)
            return "0";
        float weightF = Float.valueOf(weight);
        return String.format("%.1f", weightF / (height * height) * 10000);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("体重：" + weight + "kg\n");
        sb.append("骨骼：" + bones + "%\n");
        sb.append("脂肪：" + fat + "%\n");
        sb.append("肌肉：" + muscle + "%\n");
        sb.append("水分：" + waterRate + "%\n");
        sb.append("内脏脂肪：" + visceralFat + "\n");
        sb.append("BMR:" + bmr + "kcl");
        return sb.toString();
    }
}
