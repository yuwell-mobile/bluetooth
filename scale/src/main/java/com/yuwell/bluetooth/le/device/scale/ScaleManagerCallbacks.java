package com.yuwell.bluetooth.le.device.scale;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2016/6/27.
 */
public interface ScaleManagerCallbacks extends BleManagerCallbacks {

    void onScaleDataRead(@NonNull final BluetoothDevice device, @NonNull WeightData data);

    void onScaleError(@NonNull final BluetoothDevice device);

    void onScaleMeasureFinished(@NonNull final BluetoothDevice device);
}
