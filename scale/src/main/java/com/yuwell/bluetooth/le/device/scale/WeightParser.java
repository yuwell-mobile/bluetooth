package com.yuwell.bluetooth.le.device.scale;

import com.holtek.libHTBodyfat.HTPeopleGeneral;

import java.text.DecimalFormat;

class WeightParser {
	private static DecimalFormat dfc = new DecimalFormat("#.#");

	static WeightData parse(byte[] data) {
		// 设别类型
		int v = data[0] & 0xFF;
		String typeRec = "脂肪秤";
		if (v == 0xcf) {
			typeRec = "脂肪秤";
		} else if (v == 0xce) {
			typeRec = "人体秤";
		} else if (v == 0xcb) {
			typeRec = "婴儿秤";
		} else if (v == 0xca) {
			typeRec = "厨房秤";
		}

		// 等级和组号
		int level = (data[1] >> 4) & 0xf;
		int group = data[1] & 0xf;

		String levelRec = "普通";
		if (level == 0) {
			levelRec = "普通";
		} else if (level == 1) {
			levelRec = "业余";
		} else if (level == 2) {
			levelRec = "专业";
		}

		// 性别
		int sex = (data[2] >> 7) & 0x1;
//		String secRec = "";
//		if (sex == 1) {
//			secRec = "男";
//		} else {
//			secRec = "女";
//		}
		// 年龄
		int age = data[2] & 0x7f;

		// 身高
		int height = data[3] & 0xFF;

		// 体重
		int weight = (data[4] << 8) | (data[5] & 0xff);
		float scale = (float) 0.1;
		if (v == 0xcf) {
			scale = (float) 0.1;
		} else if (v == 0xce) {
			scale = (float) 0.1;
		} else if (v == 0xcb) {
			scale = (float) 0.01;
		} else if (v == 0xca) {
			scale = (float) 0.001;
		}

		float weightRec = scale * weight;

		if (weightRec < 0) {
			weightRec *= -1;
		}

		// 脂肪
		int zhifang = (data[6] << 8) | (data[7] & 0xff);

		float zhifangRate = (float) (zhifang * 0.1);
		// 骨骼
		int guge = data[8] & 0xff;
		float bonesWeight = guge * 0.1f;
//		float gugeRate = (float) ((guge * 0.1) / weightRec) * 100;

		// 肌肉含量
		int jirou = (data[9] << 8) | (data[10] & 0xff);
		float jirouRate = (float) (jirou * 0.1);

		// 内脏脂肪等级
		int neizanglevel = data[11] & 0xff;

		// 水份含量
		int water = data[12] << 8 | data[13];
		float waterRate = (float) (water * 0.1);

		// 热量含量
		int hot = data[14] << 8 | (data[15] & 0xff);

		int bodyAge = 0;
		if (data.length == 17) {
			bodyAge = data[16] & 0xff;
		}

		WeightData weightData = new WeightData();
		weightData.setType(typeRec);
		weightData.setLevel(levelRec);
		weightData.setSex(sex);
		weightData.setAge(age);
		weightData.setHeight(height);
		weightData.setWeight(dfc.format(weightRec < 0 ? -weightRec : weightRec));
		weightData.setFat(dfc.format(zhifangRate < 0 ? -zhifangRate : zhifangRate));
		weightData.setBones(dfc.format(bonesWeight < 0 ? -bonesWeight : bonesWeight));
		weightData.setMuscle(dfc.format(jirouRate < 0 ? -jirouRate : jirouRate));
		weightData.setVisceralFat(dfc.format(neizanglevel < 0 ? -neizanglevel : neizanglevel));
		weightData.setWaterRate(dfc.format(waterRate < 0 ? -waterRate : waterRate));
		weightData.setBmr(dfc.format(hot < 0 ? -hot : hot));
		weightData.setBodyAge(bodyAge);

		return weightData;
	}

	static WeightData parse(double weight, HTPeopleGeneral bodyfat) {
		WeightData weightData = new WeightData();
		weightData.setHeight((int) bodyfat.heightCm);
		weightData.setWeight(dfc.format(weight));
		weightData.setFat(dfc.format(bodyfat.bodyfatPercentage));
		weightData.setBones(dfc.format(bodyfat.boneKg));
		weightData.setMuscle(dfc.format(bodyfat.muscleKg));
		weightData.setVisceralFat(String.valueOf(bodyfat.VFAL));
		weightData.setWaterRate(dfc.format(bodyfat.waterPercentage));
		weightData.setBmr(String.valueOf(bodyfat.BMR));
		return weightData;
	}

	static WeightData parse(double weight) {
		WeightData weightData = new WeightData();
		weightData.setWeight(dfc.format(weight));
		weightData.setFat("0.0");
		weightData.setBones("0.0");
		weightData.setMuscle("0.0");
		weightData.setVisceralFat("0");
		weightData.setWaterRate("0.0");
		weightData.setBmr("0");
		return weightData;
	}

}
