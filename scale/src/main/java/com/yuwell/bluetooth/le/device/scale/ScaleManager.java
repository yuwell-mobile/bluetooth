package com.yuwell.bluetooth.le.device.scale;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.holtek.libHTBodyfat.HTDataType;
import com.holtek.libHTBodyfat.HTPeopleGeneral;
import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.core.YUBleManager;
import com.yuwell.bluetooth.utils.Utils;

import java.util.Calendar;

import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Body scale ble manager
 * Created by Chen on 2016/5/12.
 */
public class ScaleManager extends YUBleManager<ScaleManagerCallbacks> {

    private static final String TAG = ScaleManager.class.getSimpleName();

    private BluetoothGattCharacteristic readCharacteristic;
    private BluetoothGattCharacteristic characteristicToWrite;
    private BluetoothGattCharacteristic mBodyCompositionMeasurement;
    private BluetoothGattCharacteristic mDateTimeCharacteristic;

    private static ScaleManager managerInstance = null;

    private int gender;
    private int height;
    private int age;

    private long last;

    private ScaleManager(Context context) {
        super(context);
    }

    public static synchronized ScaleManager getInstance(final Context context) {
        return new ScaleManager(context);
    }

    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattcallback;
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
         String name = device.getName();
        if (!TextUtils.isEmpty(name)) {
            return super.connectable(device, rssi, scanRecord) &&
                    ("Electronic Scale".equals(device.getName()) || name.contains("Yuwell Scale"));
        } else {
            return false;
        }
    }

    public void poweroff() {
        if (isConnected() && characteristicToWrite != null) {
            writeCharacteristic(characteristicToWrite,
                    new byte[]{(byte) 0xfd, (byte) 0x35, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0x35}).enqueue();
        }
    }

    /**
     * Write the user data to the scale
     *
     * @param g 0-Female 1-Male
     * @param h height in cm
     * @param a age
     */
    public void writeUserInfo(int g, int h, int a) {
        gender = g;
        height = h;
        age = a;

        if (isConnected() && characteristicToWrite != null) {
            byte[] value = new byte[8];
            value[0] = (byte) (254 & 0xff);
            value[1] = 1;
            value[2] = (byte) (gender & 0xff);
            value[3] = 0;
            value[4] = (byte) (height & 0xff);
            value[5] = (byte) (age & 0xff);
            value[6] = 1;

            int check = 0;
            for (int i = 1; i < 7; i++) {
                check ^= value[i];
            }
            value[7] = (byte) (check & 0xff);

            writeCharacteristic(characteristicToWrite, value).enqueue();
        }
    }

    private final BleManagerGattCallback mGattcallback = new BleManagerGattCallback() {

        @Override
        protected void initialize() {
            super.initialize();

            setNotificationCallback(readCharacteristic).with(new DataReceivedCallback() {
                @Override
                public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                    handleWeightData(device, data);
                }
            });

            setIndicationCallback(mBodyCompositionMeasurement).with(new DataReceivedCallback() {
                @Override
                public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                    handleStandardGatt(device, data);
                }
            });

            if (readCharacteristic != null) {
                enableNotifications(readCharacteristic).enqueue();
            }
            if (mBodyCompositionMeasurement != null) {
                enableIndications(mBodyCompositionMeasurement).enqueue();
            }
            if (mDateTimeCharacteristic != null) {
                writeCharacteristic(mDateTimeCharacteristic, setDateTime()).enqueue();
            }
        }

        @Override
        protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
            BluetoothGattService mWeightService = gatt.getService(Service.WEIGHT_SCALE);
            BluetoothGattService mBodyCompositionService = gatt.getService(Service.BODY_COMPOSITION);

            if (mBodyCompositionService != null) {
                mBodyCompositionMeasurement = mBodyCompositionService.getCharacteristic(
                        Characteristic.BODY_COMPOSITION_MEASUREMENT);

                BluetoothGattService mCurrentTime = gatt.getService(Service.CURRENT_TIME);
                if (mCurrentTime != null) {
                    mDateTimeCharacteristic = mCurrentTime.getCharacteristic(Characteristic.DATE_TIME);
                }
            } else if (mWeightService != null) {
                readCharacteristic = mWeightService.getCharacteristic(Characteristic.WEIGHT_READ);
                characteristicToWrite = mWeightService.getCharacteristic(Characteristic.WEIGHT_WRITE);
            }

            return mBodyCompositionService != null || mWeightService != null;
        }

        @Override
        protected void onDeviceDisconnected() {
            readCharacteristic = null;
            characteristicToWrite = null;
            mBodyCompositionMeasurement = null;
            mDateTimeCharacteristic = null;
        }

        /**
         * 设置时间
         */
        private byte[] setDateTime() {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int min = calendar.get(Calendar.MINUTE);
            int sec = calendar.get(Calendar.SECOND);

            byte[] array = new byte[10];
            array[0] = (byte) (year & 0xFF);
            array[1] = (byte) ((year >> 8) & 0xFF);
            array[2] = (byte) (month & 0xFF);
            array[3] = (byte) (day & 0xFF);
            array[4] = (byte) (hour & 0xFF);
            array[5] = (byte) (min & 0xFF);
            array[6] = (byte) (sec & 0xFF);

            return array;
        }

        private void handleWeightData(BluetoothDevice device, Data data) {
            long current = System.currentTimeMillis();
            if (current - last < 1000) {
                Log.d(TAG, "Received 2nd time");
                mCallbacks.onScaleMeasureFinished(device);
                return;
            }
            last = current;

            final byte[] value = data.getValue();

            if (value == null) {
                return;
            }

            int head = Utils.unsignedByteToInt(value[0]);
            if (head == 0xfd) {
                if (value.length == 8) {
                    switch (Utils.unsignedByteToInt(value[7])) {
                        case 0x31:
                            writeUserInfo(gender, height, age);
                            break;
                        case 0x33:
                        case 0x35:
                            mCallbacks.onScaleError(device);
                            break;
                    }
                }
            } else {
                last = current;
                WeightData result = WeightParser.parse(value);
                mCallbacks.onScaleDataRead(device, result);
            }
        }

        private void handleStandardGatt(BluetoothDevice device, Data data) {
            long current = System.currentTimeMillis();
            if (current - last < 1000) {
                Log.d(TAG, "Received 2nd time");
                mCallbacks.onScaleMeasureFinished(device);
                return;
            }

            last = current;

            byte[] value = data.getValue();

            if (value == null) {
                return;
            }

            int weightRaw = data.getIntValue(Data.FORMAT_UINT16, 11);
            double weight = weightRaw * 0.01;

            int impedance = Utils.unsignedByteToInt(value[15]) +
                    (Utils.unsignedByteToInt(value[16]) << 8) +
                    (Utils.unsignedByteToInt(value[17]) << 16);

            HTPeopleGeneral bodyfat = new HTPeopleGeneral(weight, height, gender,
                    age, impedance);

            if (bodyfat.getBodyfatParameters() == HTDataType.ErrorNone) {
                mCallbacks.onScaleDataRead(device, WeightParser.parse(weight, bodyfat));
            } else {
                mCallbacks.onScaleDataRead(device, WeightParser.parse(weight));
            }
        }
    };
}
