package com.yuwell.bluetooth.le.device.fetus;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.core.YUBleManager;
import com.yuwell.bluetooth.utils.Utils;

import no.nordicsemi.android.ble.data.DataMerger;
import no.nordicsemi.android.ble.data.DataStream;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Created by Chen on 2020/10/30.
 */
public class FetusManager extends YUBleManager<FetusCallbacks> {

    private static final String TAG = "FetusManager";

    private BluetoothGattCharacteristic mCharacteristic;

    public FetusManager(Context context) {
        super(context);
    }

    public static boolean isSpecfiedDevice(String deviceName) {
        return !TextUtils.isEmpty(deviceName) && deviceName.contains("Heart Monitor");
    }

    public static FetusManager getInstance(final Context context) {
        return new FetusManager(context);
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
        return super.connectable(device, rssi, scanRecord) && isSpecfiedDevice(device.getName());
    }

    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(mCharacteristic).with((device, data) -> {
                if (data.size() >= 512) {
                    byte[] array = data.getValue();

                    if (array == null)
                        return;

                    FetusData fd;
                    if (array.length > 512) {
                        byte[] dataArray = new byte[512];
                        System.arraycopy(array, array.length - 512, dataArray, 0, 512);
                        fd = new FetusData(dataArray);
                    } else {
                        fd = new FetusData(array);
                    }

                    if (mCallbacks != null) {
                        mCallbacks.onHeartRateRead(fd.hr);
                        mCallbacks.onDataRead(fd);
                    }
                }
            }).merge(new DataMerger() {

                @Override
                public boolean merge(@NonNull DataStream output, @Nullable byte[] lastPacket, int index) {
                    if (lastPacket != null) {
                        output.write(lastPacket);

                        int length = lastPacket.length;
                        for (int i = length - 1; i > 1; i--) {
                            if (Utils.unsignedBytesToInt(lastPacket[i - 1], lastPacket[i]) == 0xfefe) {
                                return true;
                            }
                        }
                    }

                    return false;
                }
            });

            enableNotifications(mCharacteristic).enqueue();
        }

        @Override
        protected boolean isRequiredServiceSupported(@NonNull BluetoothGatt gatt) {
            BluetoothGattService mService = gatt.getService(Service.FFF0);
            if (mService != null) {
                mCharacteristic = mService.getCharacteristic(Characteristic.FFF1);
            }

            return mService != null;
        }

        @Override
        protected void onDeviceDisconnected() {
            mCharacteristic = null;
        }
    };
}
