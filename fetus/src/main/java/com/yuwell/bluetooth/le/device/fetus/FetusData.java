package com.yuwell.bluetooth.le.device.fetus;

import com.yuwell.bluetooth.utils.Utils;

/**
 * Created by Chen on 2020/10/30.
 */
public class FetusData {

    public int seq;
    public int sampling;
    public int bit;
    public int hr;

    public short[] shortAudioData;
    public byte[] bytesAudioData;
    public byte[] rawData;

    public FetusData(byte[] bytes) {
        shortAudioData = new short[bytes.length / 2 - 6];
        bytesAudioData = new byte[bytes.length - 12];
        rawData = bytes;
        System.arraycopy(bytes, 10, bytesAudioData, 0, bytes.length - 12);

        int offset = 2;
        seq = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]);
        offset += 2;

        sampling = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]);
        offset += 2;

        bit = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]);
        offset += 2;

        hr = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]);
        offset += 2;

        for (int i = 0; i < bytes.length - offset - 2; i += 2) {
            short val = (short) Utils.unsignedBytesToInt(bytes[i + offset], bytes[i + offset + 1]);
            shortAudioData[i / 2] = val;
            if (val <= 0x08D6) {
                bytesAudioData[i] = 0;
                bytesAudioData[i + 1] = 0;
            }
        }
    }
}
