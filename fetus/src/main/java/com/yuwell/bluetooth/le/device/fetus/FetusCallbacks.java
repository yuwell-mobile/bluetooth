package com.yuwell.bluetooth.le.device.fetus;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2020/10/30.
 */
public interface FetusCallbacks extends BleManagerCallbacks {

    void onHeartRateRead(int hr);

    void onDataRead(FetusData data);
}
