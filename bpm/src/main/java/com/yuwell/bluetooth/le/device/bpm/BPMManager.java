package com.yuwell.bluetooth.le.device.bpm;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.device.battery.BatteryManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import no.nordicsemi.android.ble.common.callback.RecordAccessControlPointDataCallback;
import no.nordicsemi.android.ble.common.callback.bps.BloodPressureMeasurementDataCallback;
import no.nordicsemi.android.ble.common.callback.bps.IntermediateCuffPressureDataCallback;
import no.nordicsemi.android.ble.common.data.RecordAccessControlPointData;
import no.nordicsemi.android.ble.common.profile.bp.BloodPressureTypes;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.ble.data.MutableData;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Blood Pressure Monitor behavior
 * Created by Chen on 2015/9/9.
 */
public class BPMManager extends BatteryManager<BPMManagerCallbacks> {

    private static final String TAG = "BPMManager";

    public static final String YUWELL_BP = "Yuwell BloodPressure";

    private BluetoothGattCharacteristic mBPMCharacteristic;
    private BluetoothGattCharacteristic mICPCharacteristic;
    private BluetoothGattCharacteristic mDateTimeCharacteristic;
    private BluetoothGattCharacteristic mCurrentTimeCharacteristic;
    private BluetoothGattCharacteristic mRecordAccessControlPointCharacteristic;
    private BluetoothGattCharacteristic mSpRx;
    private BluetoothGattCharacteristic mSpTx;

    private static final int FILTER_TYPE_USER_FACING_TIME = 2;

    private static final int OP_CODE_REPORT_STORED_RECORDS = 1;
    private static final int OP_CODE_DELETE_STORED_RECORDS = 2;
    private static final int OP_CODE_ABORT_OPERATION = 3;
    private static final int OP_CODE_RESPONSE_CODE = 6;

    private static final int OPERATOR_NULL = 0;
    private static final int OPERATOR_ALL_RECORDS = 1;
    private static final int OPERATOR_LESS_THEN_OR_EQUAL = 2;
    private static final int OPERATOR_GREATER_THEN_OR_EQUAL = 3;

    private List<BPMData> historyList = new ArrayList<>();
    private boolean readingRacp = false;
    private boolean removeFirst = false;
    private boolean racpIndicated = false;

    private static BPMManager managerInstance = null;

    private Handler mHandler = new Handler();

    private BluetoothDevice mDevice;

    private boolean clearCache = false;

    private BPMManager(Context context) {
        super(context);
    }

    /**
     * Returns the singleton implementation of BPMManager
     */
    public static synchronized BPMManager getInstance(final Context context) {
        return new BPMManager(context);
    }

    public static boolean isSpecfiedDevice(String deviceName) {
        return YUWELL_BP.equals(deviceName) ||
                (!TextUtils.isEmpty(deviceName) && deviceName.contains("Yuwell BP"));
    }

    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
        return super.connectable(device, rssi, scanRecord) && isSpecfiedDevice(device.getName());
    }

    @Override
    protected boolean shouldClearCacheWhenDisconnected() {
        return clearCache;
    }

    public void setClearCache(boolean clearCache) {
        this.clearCache = clearCache;
    }

    public void abort() {
        if (mRecordAccessControlPointCharacteristic == null) return;
        indicateRACP();
        writeCharacteristic(mRecordAccessControlPointCharacteristic, RecordAccessControlPointData.abortOperation()).enqueue();
        readingRacp = false;
    }

    /** Sends the request to obtain all records from glucose device. */
    public void getAllRecords(boolean removeFirst) {
        if (mRecordAccessControlPointCharacteristic == null) return;
        indicateRACP();

        this.removeFirst = removeFirst;
        historyList.clear();

        writeCharacteristic(mRecordAccessControlPointCharacteristic, RecordAccessControlPointData.reportAllStoredRecords())
                .done(device -> {
                    readingRacp = true;
                    mHandler.postDelayed(() -> mCallbacks.onReadHistoryFail(), 3000);
                })
                .enqueue();
    }

    /** Sends the request to delete all data from the device. */
    public void deleteAllRecords() {
        if (mRecordAccessControlPointCharacteristic == null) return;
        indicateRACP();
        writeCharacteristic(mRecordAccessControlPointCharacteristic, RecordAccessControlPointData.deleteAllStoredRecords()).enqueue();
    }

    public void getSpecificRecord(Date date, boolean greater) {
        if (mRecordAccessControlPointCharacteristic == null) return;
        indicateRACP();

        historyList.clear();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);

        int[] array = new int[7];
        array[0] = year & 0xff;
        array[1] = (year >> 8) & 0xFF;
        array[2] = month & 0xff;
        array[3] = day & 0xff;
        array[4] = hour & 0xff;
        array[5] = min & 0xff;
        array[6] = 0;

        final BluetoothGattCharacteristic characteristic = mRecordAccessControlPointCharacteristic;
        Data data = setOpCode(OP_CODE_REPORT_STORED_RECORDS,
                greater ? OPERATOR_GREATER_THEN_OR_EQUAL : OPERATOR_LESS_THEN_OR_EQUAL,
                array);

        writeCharacteristic(characteristic, data)
                .done(device -> {
                    readingRacp = true;
                    mHandler.postDelayed(() -> mCallbacks.onReadHistoryFail(), 3000);
                }).enqueue();
    }

    public void setDeviceVolume(int volume) {
        writeTxCharacteristic((byte) 0x01, (byte) (volume & 0xff));
    }

    public void changeUnit(boolean mmHg) {
        writeTxCharacteristic((byte) 0x02, (byte) (mmHg ? 0x3c : 0xc3));
    }

    public void start() {
        writeTxCharacteristic((byte) 0x03, (byte) 0x5A);
    }

    public void stop() {
        writeTxCharacteristic((byte) 0x03, (byte) 0xA5);
    }

    public void readVolume() {
        writeTxCharacteristic((byte) 0x04, (byte) 0x4d);
    }

    public void startContinuousMeasure() {
        writeTxCharacteristic((byte) 0x05, (byte) 0x1E);
    }

    public void stopContinuousMeasure() {
        writeTxCharacteristic((byte) 0x05, (byte) 0xE1);
    }

    public void startContinuousMeasureCount() {
        writeTxCharacteristic((byte) 0x06, (byte) 0x0F);
    }

    public void stopContinuousMeasureCount() {
        writeTxCharacteristic((byte) 0x06, (byte) 0xF0);
    }

    public void enterStatic() {
        writeTxCharacteristic((byte) 0x07, (byte) 0x1A);
    }

    public void exitStatic() {
        writeTxCharacteristic((byte) 0x07, (byte) 0xA1);
    }

    public void readUnit() {
        writeTxCharacteristic((byte) 0x08, (byte) 0x5f);
    }

    private void indicateRACP() {
        if (!racpIndicated) {
            enableIndications(mRecordAccessControlPointCharacteristic)
                    .done(device -> racpIndicated = true)
                    .enqueue();
        }
    }

    private void writeTxCharacteristic(byte type, byte data) {
        if (mSpTx != null) {
            byte[] cmd = new byte[3];
            cmd[0] = 0x01;
            cmd[1] = type;
            cmd[2] = data;
            writeCharacteristic(mSpTx, cmd).enqueue();
        }
    }

    private Data setOpCode(final int opCode, final int operator, final int... params) {
        return setOpCode(opCode, operator, FILTER_TYPE_USER_FACING_TIME, params);
    }

    private Data setOpCode(final int opCode, final int operator,
            final int filterType, final int... params) {
        final int size = 2 + ((params.length > 0) ? 1 : 0) + params.length;

        MutableData data = new MutableData(new byte[size]);
        // write the operation code
        int offset = 0;
        data.setValue(opCode, Data.FORMAT_UINT8, offset);
        offset += 1;

        // write the operator. This is always present but may be equal to OPERATOR_NULL
        data.setValue(operator, Data.FORMAT_UINT8, offset);
        offset += 1;

        // if parameters exists, append them. Parameters should be sorted from minimum to maximum.
        // Currently only one or two params are allowed
        if (params.length > 0) {
            data.setValue(filterType, Data.FORMAT_UINT8, offset);
            offset += 1;

            for (final Integer i : params) {
                data.setValue(i, Data.FORMAT_UINT8, offset);
                offset += 1;
            }
        }

        return data;
    }

    /**
     *
     * @param device
     * @param success 只有设备发送06 00 01 01后才认为成功。部分新款臂式机型在蓝牙断开时会发送，其他不会
     */
    private void onHistoryReadFinish(BluetoothDevice device, boolean success) {
        if (readingRacp && historyList != null && historyList.size() > 0) {
            if (removeFirst) {
                historyList.remove(0);
            }

            mCallbacks.onHistoryMeasurementsRead(device, new ArrayList<>(historyList), success);
            readingRacp = false;
        }
    }

    private final BleManagerGattCallback mGattCallback = new BatteryManagerGattCallback() {

        @Override
        protected void initialize() {
            setNotificationCallback(mICPCharacteristic)
                    .with(new IntermediateCuffPressureDataCallback() {
                        @Override
                        public void onIntermediateCuffPressureReceived(
                                @NonNull BluetoothDevice device, float cuffPressure, int unit,
                                @Nullable Float pulseRate, @Nullable Integer userID,
                                @Nullable BPMStatus status, @Nullable Calendar calendar) {
                            if (readingRacp) {
                                readingRacp = false;
                                abort();
                            }

                            if (cuffPressure == -0.1f) {
                                mCallbacks.onMeasureStart();
                            } else {
                                BPMData data = new BPMData();
                                data.sbp = (int) cuffPressure;
                                data.unit = unit;
                                data.userId = userID != null ? userID : 0;

                                if (pulseRate != null) {
                                    data.pulseRate = (int) pulseRate.floatValue();
                                }

                                if (status != null) {
                                    data.bodyMovementDetection = status.bodyMovementDetected;
                                    data.cuffFitDetection = status.cuffTooLose;
                                    data.improperMeasurementPosition = status.improperMeasurementPosition;
                                    data.irregularPulseDetection = status.irregularPulseDetected;
                                }

                                mCallbacks.onIntermediateCuffPressureRead(device, data);
                            }
                        }
                    });
            setIndicationCallback(mBPMCharacteristic)
                    .with(new BloodPressureMeasurementDataCallback() {

                        @Override
                        public void onBloodPressureMeasurementReceived(
                                @NonNull BluetoothDevice device, float systolic, float diastolic,
                                float meanArterialPressure, int unit, @Nullable Float pulseRate,
                                @Nullable Integer userID, @Nullable BPMStatus status,
                                @Nullable Calendar calendar) {
                            BPMData data = new BPMData();
                            data.sbp = (int) systolic;
                            data.dbp = (int) diastolic;
                            data.unit = unit;
                            data.meanArterialPressure = meanArterialPressure;
                            data.userId = userID != null ? userID : 0;

                            if (pulseRate != null) {
                                data.pulseRate = (int) pulseRate.floatValue();
                            }

                            if (status != null) {
                                data.bodyMovementDetection = status.bodyMovementDetected;
                                data.cuffFitDetection = status.cuffTooLose;
                                data.improperMeasurementPosition = status.improperMeasurementPosition;
                                data.irregularPulseDetection = status.irregularPulseDetected;
                            }

                            if (calendar != null) {
                                data.measureTime = calendar.getTime();
                            }

                            if (readingRacp) {
                                historyList.add(data);
                                mCallbacks.onHistoryMeasurementRead(device, data);
                                mHandler.removeCallbacksAndMessages(null);
                                // 部分机型在读取完数据后不会发送成功标记，程序判定3s后不再收到数据时读取完毕
                                mHandler.postDelayed(() -> onHistoryReadFinish(device, true),3000);
                            } else {
                                mCallbacks.onBloodPressureMeasurementRead(device, data);
                            }
                        }
                    });

            enableBatteryLevelCharacteristicNotifications();
            enableIndications(mBPMCharacteristic).enqueue();
            enableNotifications(mICPCharacteristic).enqueue();
            // 由于血压计电量发送是在连接成功后（使能一个特性后）更新，read操作可能会读取到蓝牙模块默认的数值，并非真实数值
            // 为确保能读到电量，采样监听（血压计主动发送）+ 延迟读取方式
            writeCharacteristic(getTimeWriteCharacteristic(), setDateTime())
                    .done(device -> mHandler.postDelayed(() -> readBatteryLevelCharacteristic(), 3000))
                    .enqueue();

            if (mRecordAccessControlPointCharacteristic != null) {
                setIndicationCallback(mRecordAccessControlPointCharacteristic).with(new RecordAccessControlPointDataCallback() {
                    @Override
                    public void onRecordAccessOperationCompleted(@androidx.annotation.NonNull BluetoothDevice device, int requestCode) {
                        Log.d(TAG, "onRecordAccessOperationCompleted: " + requestCode);
                        if (requestCode == OP_CODE_REPORT_STORED_RECORDS) {
                            onHistoryReadFinish(device, true);
                        }
                    }

                    @Override
                    public void onRecordAccessOperationCompletedWithNoRecordsFound(@androidx.annotation.NonNull BluetoothDevice device, int requestCode) {
                        Log.d(TAG, "onRecordAccessOperationCompletedWithNoRecordsFound: " + requestCode);
                    }

                    @Override
                    public void onNumberOfRecordsReceived(@androidx.annotation.NonNull BluetoothDevice device, int numberOfRecords) {
                        Log.d(TAG, "onNumberOfRecordsReceived: " + numberOfRecords);
                    }

                    @Override
                    public void onRecordAccessOperationError(@androidx.annotation.NonNull BluetoothDevice device, int requestCode, int errorCode) {
                        Log.d(TAG, "onRecordAccessOperationError:" + requestCode + ", error:" + errorCode);
                    }
                });
            }

            if (mSpRx != null) {
                setIndicationCallback(mSpRx).with((device, data) -> {
                    Integer type = data.getIntValue(Data.FORMAT_UINT8, 0);
                    if (type == null)
                        return;

                    if (type == 1) {
                        BloodPressureTypes.BPMStatus status = new BloodPressureTypes.BPMStatus(
                                data.getIntValue(Data.FORMAT_UINT8, 2));

                        Integer errorCode = data.getIntValue(Data.FORMAT_UINT8, 3);
                        if (errorCode != null) {
                            mCallbacks.onErrorRead(device, status, errorCode);
                        }
                    } else if (type == 2) {
                        mCallbacks.onLowBattery();
                    } else if (type == 3) {
                        Integer volume = data.getIntValue(Data.FORMAT_UINT8, 2);
                        if (volume != null) {
                            mCallbacks.onVolumeRead(device, volume);
                        }
                    } else if (type == 4) {
                        if (data.size() > 3) {
                            int value = data.getIntValue(Data.FORMAT_UINT16, 2);
                            mCallbacks.onStaticPressureRead(value);
                        }
                    } else if (type == 5) {
                        if (readingRacp) {
                            readingRacp = false;
                            abort();
                        }
                        mCallbacks.onMeasureStart();
                    } else if (type == 6) {
                        if (data.size() > 3) {
                            int value = data.getIntValue(Data.FORMAT_UINT16, 2);
                            mCallbacks.onUnitRead(value);
                        }
                    }
                });
                enableIndications(mSpRx).enqueue();
            }
        }

        @Override
        protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
            mDevice = gatt.getDevice();

            BluetoothGattService mBPService = gatt.getService(Service.BLOOD_PRESSURE_MEASUREMMENT);
            if (mBPService != null) {
                mBPMCharacteristic = mBPService.getCharacteristic(Characteristic.BLOOD_PRESSURE);
                mICPCharacteristic = mBPService.getCharacteristic(Characteristic.ICP);
                mDateTimeCharacteristic = mBPService.getCharacteristic(Characteristic.DATE_TIME);
                mRecordAccessControlPointCharacteristic = mBPService.getCharacteristic(Characteristic.RACP);
            }

            BluetoothGattService mCurrentTime = gatt.getService(Service.CURRENT_TIME);
            if (mCurrentTime != null) {
                mCurrentTimeCharacteristic = mCurrentTime.getCharacteristic(Characteristic.CURRENT_TIME);
            }

            BluetoothGattService mSPService = gatt.getService(Service.FE60);
            if (mSPService != null) {
                mSpTx = mSPService.getCharacteristic(Characteristic.SP_TX);
                mSpRx = mSPService.getCharacteristic(Characteristic.SP_RX);
            }

            return mBPMCharacteristic != null;
        }

        @Override
        protected void onDeviceDisconnected() {
            super.onDeviceDisconnected();
            onHistoryReadFinish(mDevice, false);
            mBPMCharacteristic = null;
            mICPCharacteristic = null;
            mDateTimeCharacteristic = null;
            mCurrentTimeCharacteristic = null;
            mRecordAccessControlPointCharacteristic = null;
            mSpRx = null;
            mSpTx = null;
            readingRacp = false;
            racpIndicated = false;
            mDevice = null;
            Log.d(toString(), "onDeviceDisconnected");
        }

        private BluetoothGattCharacteristic getTimeWriteCharacteristic() {
            if (mCurrentTimeCharacteristic != null) {
                return mCurrentTimeCharacteristic;
            } else if (mDateTimeCharacteristic != null) {
                return mDateTimeCharacteristic;
            }
            return null;
        }

        /**
         * 设置时间
         */
        private byte[] setDateTime() {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int min = calendar.get(Calendar.MINUTE);
            int sec = calendar.get(Calendar.SECOND);

            int byteSize = 0;
            if (mDateTimeCharacteristic != null) {
                byteSize = 7;
            }
            if (mCurrentTimeCharacteristic != null) {
                byteSize = 10;
            }

            byte[] array = new byte[byteSize];
            array[0] = (byte) (year & 0xFF);
            array[1] = (byte) ((year >> 8) & 0xFF);
            array[2] = (byte) (month & 0xFF);
            array[3] = (byte) (day & 0xFF);
            array[4] = (byte) (hour & 0xFF);
            array[5] = (byte) (min & 0xFF);
            array[6] = (byte) (sec & 0xFF);

            return array;
        }
    };
}
