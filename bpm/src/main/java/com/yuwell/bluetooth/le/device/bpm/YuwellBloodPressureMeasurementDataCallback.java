package com.yuwell.bluetooth.le.device.bpm;

import android.bluetooth.BluetoothDevice;

import androidx.annotation.NonNull;

import no.nordicsemi.android.ble.common.callback.bps.BloodPressureMeasurementDataCallback;
import no.nordicsemi.android.ble.data.Data;

/**
 * Created by Chen on 2020/12/8.
 */
public abstract class YuwellBloodPressureMeasurementDataCallback extends BloodPressureMeasurementDataCallback {

    @Override
    public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
        // First byte: flags
        int offset = 0;
        final int flags = data.getIntValue(Data.FORMAT_UINT8, offset++);
        final boolean timestampPresent         = (flags & 0x02) != 0;
        final boolean pulseRatePresent         = (flags & 0x04) != 0;
        final boolean userIdPresent            = (flags & 0x08) != 0;
        final boolean measurementStatusPresent = (flags & 0x10) != 0;
        final boolean errorPresent             = (flags & 0x20) != 0;

        if (errorPresent) {
            if (data.size() < 7
                    + (timestampPresent ? 7 : 0) + (pulseRatePresent ? 2 : 0)
                    + (userIdPresent ? 1 : 0) + (measurementStatusPresent ? 2 : 0)) {
                onInvalidDataReceived(device, data);
                return;
            }

            offset += 6;

            if (timestampPresent) {
                offset += 7;
            }

            if (pulseRatePresent) {
                offset += 2;
            }

            if (userIdPresent) {
                offset += 1;
            }

            if (measurementStatusPresent) {
                final int errorCode = data.getIntValue(Data.FORMAT_UINT8, offset);
                onErrorReceived(errorCode);
            }
        } else {
            super.onDataReceived(device, data);
        }
    }

    public abstract void onErrorReceived(int errorCode);
}
