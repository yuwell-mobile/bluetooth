/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.yuwell.bluetooth.le.device.bpm;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import com.yuwell.bluetooth.le.device.battery.BatteryManagerCallbacks;

import java.util.List;

import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.ble.common.profile.bp.BloodPressureTypes;

public interface BPMManagerCallbacks extends BleManagerCallbacks, BatteryManagerCallbacks {

	/**
	 * Called when new BPM value has been obtained from the sensor
	 */
	void onBloodPressureMeasurementRead(@NonNull final BluetoothDevice device, @NonNull final BPMData bpmData);

	/**
	 * Called when new ICP value has been obtained from the device
	 */
	void onIntermediateCuffPressureRead(@NonNull final BluetoothDevice device, BPMData data);

	void onMeasureStart();

	void onErrorRead(@NonNull final BluetoothDevice device, BloodPressureTypes.BPMStatus status, int errorCode);

	void onHistoryMeasurementsRead(@NonNull final BluetoothDevice device, List<BPMData> dataList, boolean success);

	default void onVolumeRead(BluetoothDevice device, int volume) {}

	default void onStaticPressureRead(int pressure) {}

	default void onLowBattery() {}

	/**
	 * On unit read
	 * @param unit 1:mmHg 2:kPa
	 */
	default void onUnitRead(int unit) {}

	default void onHistoryMeasurementRead(@NonNull final BluetoothDevice device, @NonNull final BPMData bpmData) {

	}

	default void onReadHistoryFail() {

	}
}
