package com.yuwell.bluetooth.le.device.bpm;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chen on 2018/5/31.
 */
public class BPMData {

	public int sbp; // ICP时作为中间袖带压力
	public int dbp;
	public float meanArterialPressure;
	public int pulseRate;
	public Date measureTime;
	public int userId;
	public Boolean bodyMovementDetection;
	public Boolean cuffFitDetection;
	public Boolean irregularPulseDetection;
	public Boolean improperMeasurementPosition;
	public int state;
	public int unit;

	public String toJsonString() {
		Map<String, Object> map = new HashMap<>();
		map.put("sbp", sbp);
		map.put("dbp", dbp);
		map.put("pulseRate", pulseRate);
		map.put("time", measureTime.getTime());
		map.put("userId", userId);
		map.put("bodyMovementDetection", String.valueOf(bodyMovementDetection));
		map.put("cuffFitDetection", String.valueOf(cuffFitDetection));
		map.put("irregularPulseDetection", String.valueOf(irregularPulseDetection));
		map.put("improperMeasurementPosition", String.valueOf(improperMeasurementPosition));
		map.put("unit", String.valueOf(unit));

		return new JSONObject(map).toString();
	}
}
