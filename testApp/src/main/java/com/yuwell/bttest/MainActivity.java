package com.yuwell.bttest;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.yuwell.bluetooth.le.core.BleService;
import com.yuwell.bluetooth.le.core.CallbacksHandler;
import com.yuwell.bluetooth.le.device.bpm.BPMData;
import com.yuwell.bluetooth.le.device.bpm.BPMManager;
import com.yuwell.bluetooth.le.device.bpm.BPMManagerCallbacks;
import com.yuwell.bluetooth.le.device.chol.CholManager;
import com.yuwell.bluetooth.le.device.chol.CholManagerCallbacks;
import com.yuwell.bluetooth.le.device.chol.CholesterolData;
import com.yuwell.bluetooth.le.device.gls.GlucoseData;
import com.yuwell.bluetooth.le.device.gls.GlucoseManager;
import com.yuwell.bluetooth.le.device.gls.GlucoseManagerCallbacks;
import com.yuwell.bluetooth.le.device.hts.HTSManagerCallbacks;
import com.yuwell.bluetooth.le.device.hts.ThermometerManager;
import com.yuwell.bluetooth.le.device.oxi.BloodOxygenData;
import com.yuwell.bluetooth.le.device.oxi.OximeterManager;
import com.yuwell.bluetooth.le.device.oxi.OximeterManagerCallbacks;
import com.yuwell.bluetooth.le.device.pm.PneumatoMeter;
import com.yuwell.bluetooth.le.device.pm.PneumatorData;
import com.yuwell.bluetooth.le.device.pm.PneumatorMeterCallbacks;
import com.yuwell.bluetooth.le.device.scale.ScaleManager;
import com.yuwell.bluetooth.le.device.scale.ScaleManagerCallbacks;
import com.yuwell.bluetooth.le.device.scale.WeightData;
import com.yuwell.bluetooth.le.device.ua.UACholData;
import com.yuwell.bluetooth.le.device.ua.UACholManagerCallbacks;
import com.yuwell.bluetooth.le.device.urine.UAManagerCallbacks;
import com.yuwell.bluetooth.le.device.urine.UrineData;

import java.text.SimpleDateFormat;
import java.util.List;

import no.nordicsemi.android.ble.common.profile.bp.BloodPressureTypes;

public class MainActivity extends AppCompatActivity {

	private static final String TAG = "MainActivity";
	private static final int PERMISSIONS_REQUEST_LOCATION = 0;

	private TextView mLog;
	private ScrollView mScrollView;
	private MenuItem menuRead;

	private BleService.LocalBinder serviceBinder;

	private PneumatoMeter mUAManager;
	private CholManager mCholManager;
	private ScaleManager scaleManager;
	private BPMManager mBPMManager;
	private String type = "bp";

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Callback mCallbacksHandler = new Callback();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		checkPermission();
		initViews();

		mBPMManager = BPMManager.getInstance(getApplicationContext());
		mUAManager = PneumatoMeter.getInstance(getApplicationContext());
		mCholManager = CholManager.getInstance(getApplicationContext());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(mServiceConnection);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		menuRead = menu.findItem(R.id.menu_read);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_bpm: // 血压计
				type = "bp";
				menuRead.setVisible(true);
				serviceBinder.setBleManager(mBPMManager, mCallbacksHandler);
				return true;
			case R.id.menu_glu: // 血糖仪
				menuRead.setVisible(false);
				serviceBinder.setBleManager(GlucoseManager.getInstance(getApplicationContext()), mCallbacksHandler);
				return true;
			case R.id.menu_chol: // 血脂仪
				type = "chol";
				serviceBinder.setBleManager(mCholManager, mCallbacksHandler);
				menuRead.setVisible(true);
				return true;
			case R.id.menu_oxy:
				menuRead.setVisible(false); // 血氧仪
				serviceBinder.setBleManager(OximeterManager.getInstance(getApplicationContext()), mCallbacksHandler);
				return true;
			case R.id.menu_temp:
				menuRead.setVisible(false); // 体温计
				serviceBinder.setBleManager(ThermometerManager.getInstance(getApplicationContext()), mCallbacksHandler);
				return true;
			case R.id.menu_urine:
				type = "urine";
				serviceBinder.setBleManager(mUAManager, mCallbacksHandler);
				menuRead.setVisible(true); // 尿液分析仪
				return true;
			case R.id.menu_read:
				switch (type) {
					case "chol":
						mCholManager.readCholData(); // 读取血脂仪测量结果
						break;
					case "urine":
						mUAManager.readData(); // 读取尿液分析仪测量结果
						break;
					case "bp":
						mBPMManager.getAllRecords(false);
						break;
				}
				return true;
			case R.id.menu_scale:
				scaleManager = ScaleManager.getInstance(getApplicationContext());
				serviceBinder.setBleManager(scaleManager, mCallbacksHandler);
				return true;
			case R.id.menu_clear:
				mLog.setText(null);
				return true;
			default:
				menuRead.setVisible(false);
				return super.onOptionsItemSelected(item);
		}
	}

	private void initViews() {
		mLog = findViewById(R.id.status);
		mLog.setMaxLines(Integer.MAX_VALUE);
		mScrollView = findViewById(R.id.scrollView);
	}

	private void appendLog(String text) {
		mLog.setText(mLog.getText() + "\n" + text);
	}

	private void checkPermission() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED) {

			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
					PERMISSIONS_REQUEST_LOCATION);
		} else {
			bindBleService();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
			@NonNull int[] grantResults) {
		if (requestCode == PERMISSIONS_REQUEST_LOCATION) {
			if (grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				bindBleService();
			} else {
				Log.e(TAG, "Permission denied!");
				finish();
			}
		}
	}

	private void bindBleService() {
		Intent intent = new Intent(this, BleService.class);
		bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
	}

	private class Callback extends CallbacksHandler.DefaultCallbacks implements
			BPMManagerCallbacks, GlucoseManagerCallbacks, OximeterManagerCallbacks,
			CholManagerCallbacks, UAManagerCallbacks, UACholManagerCallbacks, HTSManagerCallbacks,
			PneumatorMeterCallbacks, ScaleManagerCallbacks {

		@Override
		public void onDeviceConnected(BluetoothDevice device) {
			showData("Device connected, " + device.getName() + " " + device.getAddress());
		}

		@Override
		public void onDeviceDisconnected(BluetoothDevice device) {
			showData("Device disconnected");
			serviceBinder.scanBleDevice();
		}

		@Override
		public void onDeviceReady(BluetoothDevice device) {
			showData("Gatt init done");
			mUAManager.setParameter(true, 20, 170);
			if (scaleManager != null) {
				scaleManager.writeUserInfo(1, 177, 29);
			}
		}

		@Override
		public void onBloodPressureMeasurementRead(@NonNull BluetoothDevice device, BPMData bpmData) {
			showData(sdf.format(bpmData.measureTime) + " " + bpmData.toJsonString());
		}

		@Override
		public void onIntermediateCuffPressureRead(@NonNull BluetoothDevice device, BPMData data) {
			showData("cuff: " + data.sbp);
		}

		@Override
		public void onMeasureStart() {
			showData("BPM measure started");
		}

		@Override
		public void onErrorRead(@NonNull BluetoothDevice device, BloodPressureTypes.BPMStatus status, int errorCode) {
			showData("error: " + errorCode);
		}

		@Override
		public void onHistoryMeasurementsRead(@NonNull BluetoothDevice device, List<BPMData> dataList, boolean success) {
			for (BPMData bpmData : dataList) {
				showData(sdf.format(bpmData.measureTime) + " " + bpmData.toJsonString());
			}
		}

		@Override
		public void onGlucoseMeasurementRead(@NonNull BluetoothDevice device, GlucoseData data) {
			showData(sdf.format(data.time) + " " + data.toJsonString());
		}

		@Override
		public void onGlucoseErrorRead(@NonNull BluetoothDevice device, int errorCode) {

		}

		@Override
		public void onBloodTook() {

		}

		@Override
		public void onHistoryGlucoseMeasurementsRead(@NonNull BluetoothDevice device, List<GlucoseData> data) {

		}

		@Override
		public void onOxygenDataRead(@NonNull BluetoothDevice device, BloodOxygenData data) {
			showData(data.toJsonString());
		}

		@Override
		public void onPulseWaveRead(@NonNull BluetoothDevice device,
				int pulseWave) {

		}

		@Override
		public void onSensorOff(@NonNull BluetoothDevice device) {

		}

		@Override
		public void onCholRead(@NonNull BluetoothDevice device, CholesterolData data) {
			showData(sdf.format(data.time) + " " + data.toJsonString());
		}

		@Override
		public void onUrineDataRead(@NonNull BluetoothDevice device, UrineData data) {
			showData(sdf.format(data.time) + " " + data.toString());
		}

		@Override
		public void onUADataRead(@NonNull BluetoothDevice device, UACholData data) {
			showData(sdf.format(data.time) + " " + data.toJsonString());
		}

		@Override
		public void onHTValueReceived(@NonNull BluetoothDevice device, double value, int unit) {
			showData("Temperature:" + value + ", unit:" + unit);
		}

		@Override
		public void onBatteryValueRead(@NonNull BluetoothDevice device, int value) {
			showData("battery read:" + value);
		}

		@Override
		public void onBatteryValueNotified(@NonNull BluetoothDevice device, int value) {
			showData("battery notify:" + value);
		}

		private void showData(final String text) {
			runOnUiThread(() -> {
				appendLog(text);
				mScrollView.fullScroll(View.FOCUS_DOWN);
			});
		}

		@Override
		public void onDataRead(List<PneumatorData> list) {
			for (PneumatorData data : list) {
				showData(data.toString());
			}
		}


		@Override
		public void onScaleDataRead(@NonNull BluetoothDevice device, @NonNull WeightData data) {
			showData(data.toString());
		}

		@Override
		public void onScaleError(@NonNull BluetoothDevice device) {
			showData("scale error");
		}

		@Override
		public void onScaleMeasureFinished(@NonNull BluetoothDevice device) {

		}
	}

	/**
	 * Service连接回调函数
     */
	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName, IBinder service) {
			serviceBinder = (BleService.LocalBinder) service;

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
				serviceBinder.setIntervals(20, 5);
			}

			/*扫描指定MAC设备*/
			/*List<ScanFilter> filters = new ArrayList<>();
			filters.add(new ScanFilter.Builder().setDeviceAddress("84:DD:20:E3:AE:55").build());
			serviceBinder.setScanFilters(filters);*/

			try {
				serviceBinder.setBleManager(mBPMManager, mCallbacksHandler);
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Activity创建时判断当前蓝牙服务连接状态
			// connection state 参见#android.bluetooth.BluetoothProfile四种状态
			int state = serviceBinder.getConnectionState();
			appendLog("State:" + state);
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			serviceBinder = null;
		}
	};
}
