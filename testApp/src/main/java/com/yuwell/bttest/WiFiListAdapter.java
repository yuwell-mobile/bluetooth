package com.yuwell.bttest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuwell.bluetooth.le.device.respirator.ScanObj;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2021/2/22.
 */
public class WiFiListAdapter extends BaseAdapter {

    private List<ScanObj> data = new ArrayList<>();

    private LayoutInflater mInflater;

    public WiFiListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public ScanObj getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        WiFiVH vh;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_wifi, parent, false);
            vh = new WiFiVH(convertView);
            convertView.setTag(vh);
        } else {
            vh = (WiFiVH) convertView.getTag();
        }

        ScanObj obj = data.get(position);
        vh.mTextSsid.setText(obj.getSSID());
        vh.mTextMac.setText(obj.getMac());
        vh.mImgStrength.setImageResource(parseWifiStrength(obj.getRssi()));
        vh.mImgEncrypt.setVisibility(obj.getEncrpytType() != 0 ? View.VISIBLE : View.GONE);

        return convertView;
    }

    public void updateData(List<ScanObj> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public static class WiFiVH {

        TextView mTextSsid;
        TextView mTextMac;
        ImageView mImgStrength;
        ImageView mImgEncrypt;

        public WiFiVH(View v) {
            mTextSsid = v.findViewById(R.id.text_ssid);
            mTextMac = v.findViewById(R.id.text_mac);
            mImgStrength = v.findViewById(R.id.img_wifi);
            mImgEncrypt = v.findViewById(R.id.img_secure);
        }
    }

    public int parseWifiStrength(int rssi){
        int wifiStrength;

        int highRssi = -35;
        int lowRssi  = -65;

        int high_low_interval = highRssi - lowRssi;
        int middle1 = lowRssi + high_low_interval/6; // 1/6
        int middle2 = middle1 + high_low_interval/3; // 1/2
//		int middle3 = middle2 + high_low_interval/6;

        if(rssi < lowRssi){
            wifiStrength = R.drawable.strength_grade1;
        } else if(rssi >= lowRssi && rssi < middle1){
            wifiStrength = R.drawable.strength_grade2;
        } else if(rssi >= middle1 && rssi < middle2){
            wifiStrength = R.drawable.strength_grade3;
        } else if(rssi >= middle2 && rssi < highRssi){
            wifiStrength = R.drawable.strength_grade4;
        } else {
            wifiStrength = R.drawable.strength_grade0;
        }

        return wifiStrength;
    }
}
