package com.yuwell.bttest;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.yuwell.bluetooth.le.core.BleService;
import com.yuwell.bluetooth.le.core.CallbacksHandler;
import com.yuwell.bluetooth.le.device.respirator.RespiratorCallbacks;
import com.yuwell.bluetooth.le.device.respirator.RespiratorManager;
import com.yuwell.bluetooth.le.device.respirator.ScanObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class WifiConfigActivity extends AppCompatActivity {

	private static final String TAG = "WifiConfigActivity";
	private static final int PERMISSIONS_REQUEST_LOCATION = 0;

	private TextView mLog;
	private ScrollView mScrollView;
	private ListView mListView;
	private MenuItem menuRead;

	private WiFiListAdapter mAdapter;

	private BleService.LocalBinder serviceBinder;

	private RespiratorManager mRM;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Callback mCallbacksHandler = new Callback();

	private Set<ScanObj> apSets = new ArraySet<>();

	private int scanWifiCount = 0;
	private int type = -1;

	private AlertDialog alertDialog_pwd = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wifi);
		checkPermission();
		initViews();

		mRM = new RespiratorManager(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(mServiceConnection);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		menuRead = menu.findItem(R.id.menu_read);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_read:
				mRM.startWifiConfig();
				return true;
		}
		return false;
	}

	private void initViews() {
		mLog = findViewById(R.id.status);
		mLog.setMaxLines(Integer.MAX_VALUE);
		mScrollView = findViewById(R.id.scrollView);
		mListView = findViewById(R.id.list_view);
		mAdapter = new WiFiListAdapter(this);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener((parent, view, position, id) -> {
			showAPConnectDialog(mAdapter.getItem(position));
		});
	}

	private void appendLog(String text) {
		mLog.setText(mLog.getText() + "\n" + text);
	}

	private void checkPermission() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED) {

			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
					PERMISSIONS_REQUEST_LOCATION);
		} else {
			bindBleService();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
			@NonNull int[] grantResults) {
		if (requestCode == PERMISSIONS_REQUEST_LOCATION) {
			if (grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				bindBleService();
			} else {
				Log.e(TAG, "Permission denied!");
				finish();
			}
		}
	}

	private void bindBleService() {
		Intent intent = new Intent(this, BleService.class);
		bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
	}

	private void scanWifi(int type) {
		if (type == RespiratorCallbacks.NULL)
			return;

		mRM.scanWifi(type == RespiratorCallbacks.WLAN_TYPE_2G);
		++scanWifiCount;
	}

	public void showAPConnectDialog(ScanObj scanObj){ //tag0:BT AP LIST  tag1:Phone AP LIST

		//final byte sendBand = band;
		final byte sendBand = scanObj.getBand();
		final byte encryptType = scanObj.getEncrpytType();
		final String sendSSID = scanObj.getSSID();
		final String sendMAC = scanObj.getMac();

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		final EditText edittext = new EditText(this);

		//check security type
		if(encryptType == 1 || encryptType == 2){// 1:WPA/WPA2 Key  2:WEP Key
			String savedPwd = "";

			edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			edittext.setText(savedPwd);
			alertDialogBuilder.setTitle("Enter the password for AP \"" + sendSSID + "\"");

			alertDialogBuilder.setView(edittext);
		} else if (encryptType ==  0) {//open
			alertDialogBuilder.setTitle("The target AP \"" + sendSSID + "\" has no security. Suggest choosing an secured AP.");
		}

		alertDialogBuilder.setCancelable(false);
		alertDialogBuilder.setPositiveButton("Continue",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				//hide keyboard
				if (edittext != null) {
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
				}

				boolean isAPProfileRight = false;
				String passwordStr = null;
				byte sendEncrypt = 1;

				if (encryptType == 0) {	/* AP is open */
					sendEncrypt = 0;
					isAPProfileRight = true;
				}else{					/* AP is not open */
					if(encryptType == 1){
						sendEncrypt = 1;
					}else if(encryptType == 2){
						sendEncrypt = 2;
					}
					passwordStr = edittext.getText().toString();
					// Check the legality of the input password
					int checkPwdFlag = APPasswordCheck.checkWifiPassWord(passwordStr.getBytes(),encryptType);

					if(checkPwdFlag == 1){/* password in right format */
						isAPProfileRight = true;
					}else{/* password in wrong format */
						isAPProfileRight = false;

						if(checkPwdFlag  ==  0 && encryptType == 1){
							Log.e(TAG,"WPA_WRONG_PASSWORD_FORMAT");
							//ToastOps.getToast( "Invalid WPA/WPA2 Key! Key Length Must >=8 characters").show();
						}else if(checkPwdFlag == 0 && encryptType == 2){
							Log.e(TAG,"WEP_WRONG_PASSWORD_FORMAT");
							//ToastOps.getToast( "Invalid WEP Key! Key Length Must Be 5,10,13,26 characters").show();
						}else{
							Log.e(TAG,"Invalid Key");
							//ToastOps.getToast( "Invalid Key!").show();
						}

						AlertDialog.Builder builder = new AlertDialog.Builder(WifiConfigActivity.this);
						builder.setTitle("Warning");
						builder.setMessage("The password is invalid!");
						builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}

						});
						builder.create().show();

					}// wrong password format
				}

				if(isAPProfileRight){
					mRM.sendAPProfile(sendBand, sendEncrypt, sendSSID, sendMAC, passwordStr);
				}
			}
		});


		alertDialogBuilder.setNegativeButton("Back",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		});

		alertDialog_pwd = alertDialogBuilder.create();
		alertDialog_pwd.show();
	}

	private class Callback extends CallbacksHandler.DefaultCallbacks implements RespiratorCallbacks {

		@Override
		public void onDeviceConnecting(BluetoothDevice device) {
			if (device.getBondState() == BluetoothDevice.BOND_NONE) {
				mRM.createBond();
			}
		}

		@Override
		public void onDeviceConnected(BluetoothDevice device) {
			showData("Device connected, " + device.getName() + " " + device.getAddress());
		}

		@Override
		public void onDeviceDisconnected(BluetoothDevice device) {
			showData("Device disconnected");
			serviceBinder.scanBleDevice();
		}

		@Override
		public void onDeviceReady(BluetoothDevice device) {
			showData("Gatt init done");
		}

		private void showData(final String text) {
			runOnUiThread(() -> {
				appendLog(text);
				mScrollView.fullScroll(View.FOCUS_DOWN);
			});
		}

		@Override
		public void onWLANBandRead(int type) {
			showData("onWLANBandRead: " + type);
			WifiConfigActivity.this.type = type;
			scanWifi(type);
		}

		@Override
		public void onWLANResultsRead(@Nullable List<ScanObj> list) {
			if (list != null) {
				apSets.addAll(list);
			}

			mAdapter.updateData(new ArrayList<>(apSets));

			if (scanWifiCount < 3) {
				scanWifi(type);
			} else {
				scanWifiCount = 0;
			}
		}

		@Override
		public void onHomeAPRead(int type, int encryptType) {

		}

		@Override
		public void onWLANProfileSent(boolean success) {
			showData("onWLANProfileSent: " +  success);
			if (success) {
				mRM.queryRepeaterStatus();
			}
		}

		@Override
		public void onRepeaterStatusRead(boolean success) {
			showData("onRepeaterStatusRead: " + success);
		}
	}

	/**
	 * Service连接回调函数
     */
	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName, IBinder service) {
			serviceBinder = (BleService.LocalBinder) service;

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
				serviceBinder.setIntervals(20, 5);
			}

			/*扫描指定MAC设备*/
			/*List<ScanFilter> filters = new ArrayList<>();
			filters.add(new ScanFilter.Builder().setDeviceAddress("84:DD:20:E3:AE:55").build());
			serviceBinder.setScanFilters(filters);*/

			try {
				serviceBinder.setBleManager(mRM, mCallbacksHandler);
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Activity创建时判断当前蓝牙服务连接状态
			// connection state 参见#android.bluetooth.BluetoothProfile四种状态
			int state = serviceBinder.getConnectionState();
			appendLog("State:" + state);
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			serviceBinder = null;
		}
	};
}
