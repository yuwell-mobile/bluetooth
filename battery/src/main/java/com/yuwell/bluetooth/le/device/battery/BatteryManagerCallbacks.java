package com.yuwell.bluetooth.le.device.battery;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import no.nordicsemi.android.ble.BleManagerCallbacks;

public interface BatteryManagerCallbacks extends BleManagerCallbacks {

	void onBatteryValueRead(@NonNull final BluetoothDevice device, final int value);

	default void onBatteryValueNotified(@NonNull final BluetoothDevice device, final int value) {};
}
