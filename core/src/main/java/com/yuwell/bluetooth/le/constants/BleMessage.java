package com.yuwell.bluetooth.le.constants;

/**
 * Notify user about ble messages
 * Created by Chen on 2015/9/9.
 */
public interface BleMessage {

    int STATE_CHANGE = 0x1000;

    int DEVICE_READY = 0x1200;

    int BP_DATA = 0x1001;

    int BG_DATA = 0x1002;

    int ICP_DATA = 0x1003;

    int BATTERY = 0x1004;

    int OXI_DATA = 0x1005;

    int CHOLE_DATA = 0x1006;

    int URINE_DATA = 0x1007;

    int THERMO_DATA = 0x1008;

    int WEIGHT_DATA = 0x1009;

    int WEIGHT_ERROR = 0x1010;

    int WEIGHT_SECOND = 0x1011;

    int DEVICE_FOUND = 0x1012;

    int UART_DATA = 0x1013;

    String ACTION_BLE = "com.yuwell.bluetooth.ACTION_BLE";
}
