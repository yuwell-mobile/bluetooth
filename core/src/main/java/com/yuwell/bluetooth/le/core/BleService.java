package com.yuwell.bluetooth.le.core;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Keep;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;

/**
 * A service which controls ble devices
 * Created by Chen on 2015/9/9.
 * On some devices, the scan result may differ, see <url>https://stackoverflow.com/a/29731725/5144446</url>
 */
@SuppressLint("NewApi")
public class BleService extends Service {

    private static final String TAG = BleService.class.getSimpleName();

    private BluetoothAdapter mBluetoothAdapter;

    protected YUBleManager mBleManager;
    protected BleScanner scanner;

    private Handler mHandler;

    private final Object mLock = new Object();

    public class LocalBinder extends Binder {
        public BleService getService() {
            return BleService.this;
        }

        public boolean setBleManager(YUBleManager manager) {
            return setManager(manager);
        }

        public boolean setBleManager(YUBleManager manager, BleManagerCallbacks callbacks) {
        	return setManager(manager, callbacks);
		}

        public int getConnectionState() {
            return BleService.this.getConnectionState();
        }

        /**
         * Start to scan ble devices after 500ms
         */
        public void scanBleDevice(boolean autoEnable) {
            startScan(autoEnable);
        }

        public void scanBleDevice() {
            startScan(false);
        }

        public void stopScanBleDevice() {
            stopScan();
        }

        /**
         * Disconnect current connection
         */
        public void disconnect() {
            if (mBleManager != null && mBleManager.isConnected()) {
                mBleManager.disconnect();
            }
        }

        /**
         * Scan interval in seconds
         * @param scanInterval
         * @param pauseInterval
         */
        public void setIntervals(int scanInterval, int pauseInterval) {
            scanner.setIntervals(scanInterval, pauseInterval);
        }

        public BluetoothDevice getDevice() {
			if (mBleManager != null) {
				return mBleManager.getBluetoothDevice();
			} else {
				return null;
			}
        }

        public void setScanFilters(List<ScanFilter> scanFilters) {
            if (scanner != null) {
                scanner.setFilters(scanFilters);
            }
        }
    }

    private final CallbacksHandler.EventBusCallback bleServiceBehavior = new CallbacksHandler.EventBusCallback() {

        @Override
        public void onDeviceDisconnected(BluetoothDevice device) {
            super.onDeviceDisconnected(device);
            restartScanIfNecessary();
        }

        private void restartScanIfNecessary() {
			scanner.restartScan();
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        // For API level 18 and above, get a reference to BluetoothAdapter
        // through BluetoothManager.
        BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (mBluetoothManager == null) {
            Log.e(TAG, "Unable to initialize BluetoothManager.");
            return;
        } else {
            mBluetoothAdapter = mBluetoothManager.getAdapter();
            if (mBluetoothAdapter == null) {
                Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
                return;
            }
        }

        scanner = new BleScanner(this, scanCallBack);
        mHandler = new Handler(getMainLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        enableBluetoothAdapter();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mBleManager != null) {
            mBleManager.close();
            mBleManager = null;
        }

        scanner.stopScan();
        scanner = null;

        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }

        mBluetoothAdapter = null;
        Log.i(TAG, "Service destroyed");
    }

    private void enableBluetoothAdapter() {
        mBluetoothAdapter.enable();
    }

    public final int getConnectionState() {
        return mBleManager == null ? BluetoothProfile.STATE_DISCONNECTED : mBleManager.getConnectionState();
    }

    private final ScanCallback scanCallBack = new ScanCallback() {

        private List<ScanResult> scanResults;

        @Keep
        @Override
        public void onScanResult(final int callbackType, final ScanResult result) {
            // do nothing
            Log.d(TAG, "onScanResult: " + result);
        }

        @SuppressLint("MissingPermission")
        @Keep
        @Override
        public void onBatchScanResults(final List<ScanResult> results) {
            // To avoid dead lock in scanner, do in other method
            this.scanResults = new ArrayList<>(results);

            ScanResult scanResult = null;
            for (ScanResult result : scanResults) {
                Log.d(TAG, "onBatchScanResults(), Device=" + result.getDevice()
                        + ", mDeviceName=" + (result.getScanRecord() != null ? result.getScanRecord().getDeviceName() : result.getDevice().getName())
                        + ", mRssi=" + result.getRssi());

                if (onDeviceScanned(result)) {
                    scanResult = result;
                    break;
                }
            }

            if (scanResult != null) {
                final BluetoothDevice device = scanResult.getDevice();
                synchronized (mLock) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scanner.stopScan();
                        }
                    }, 100);
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mBleManager != null) {
                                mBleManager.connect(device)
                                        .useAutoConnect(false)
                                        .retry(3, 100)
                                        .enqueue();
                            }
                        }
                    }, 400);
                }
            }
        }

        @Keep
        @Override
        public void onScanFailed(final int errorCode) {
            // should never be called
        }
    };

    protected boolean onDeviceScanned(ScanResult result) {
        return mBleManager != null && !mBleManager.isConnected() &&
                mBleManager.connectable(result.getDevice(), result.getRssi(), result.getScanRecord());
    }

    protected final boolean setManager(YUBleManager manager) {
        return setManager(manager, bleServiceBehavior);
    }

    protected final boolean setManager(YUBleManager manager, BleManagerCallbacks callbacks) {
        if (mBleManager != null && mBleManager.equals(manager) && mBleManager.isConnected()) {
            return false;
        } else {
            if (mBleManager != null && mBleManager.isConnected()) {
                mBleManager.close();
            }

            mBleManager = manager;
            if (manager != null) {
                manager.setGattCallbacks(callbacks);
                scanner.startScan(false);
            }
            return true;
        }
    }

    protected final void stopScan() {
        scanner.stopScan();
    }

    protected final void startScan(boolean autoEnable) {
        scanner.startScan(autoEnable);
    }
}
