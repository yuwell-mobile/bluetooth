package com.yuwell.bluetooth.le.core;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.os.Message;

import com.yuwell.bluetooth.le.constants.BleMessage;

import org.greenrobot.eventbus.EventBus;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Compatible with both event callback and listener callback
 * Created by Chen on 2016/6/27.
 */
public class CallbacksHandler {

    public static class DefaultCallbacks implements BleManagerCallbacks {

        @Override
        public void onDeviceConnecting(BluetoothDevice device) {}

        @Override
        public void onDeviceConnected(BluetoothDevice device) {}

        @Override
        public void onDeviceDisconnecting(BluetoothDevice device) {}

        @Override
        public void onDeviceDisconnected(BluetoothDevice device) {}

        @Override
        public void onLinkLossOccurred(BluetoothDevice device) {}

        @Override
        public void onServicesDiscovered(BluetoothDevice device,
                boolean optionalServicesFound) {}

        @Override
        public void onDeviceReady(BluetoothDevice device) {}

        @Override
        public void onBondingRequired(BluetoothDevice device) {}

        @Override
        public void onBonded(BluetoothDevice device) {}

        @Override
        public void onBondingFailed(BluetoothDevice device) {}

        @Override
        public void onError(BluetoothDevice device,  String message,
                int errorCode) {}

        @Override
        public void onDeviceNotSupported(BluetoothDevice device) {}

    }

    public static class EventBusCallback extends DefaultCallbacks {

        @Override
        public void onDeviceConnected(BluetoothDevice device) {
            postConnectionState(BluetoothProfile.STATE_CONNECTED);
        }

        @Override
        public void onDeviceDisconnected(BluetoothDevice device) {
            postConnectionState(BluetoothProfile.STATE_DISCONNECTED);
        }

        @Override
        public void onDeviceConnecting(BluetoothDevice device) {
            postConnectionState(BluetoothProfile.STATE_CONNECTING);
        }

        @Override
        public void onDeviceReady(BluetoothDevice device) {
            postMessage(BleMessage.DEVICE_READY);
        }

        private static void postConnectionState(int state) {
            postMessage(BleMessage.STATE_CHANGE, state);
        }

        private static void postMessage(int what) {
            Message event = new Message();
            event.what = what;
            EventBus.getDefault().post(event);
        }

        private static void postMessage(int what, int arg) {
            Message event = new Message();
            event.what = what;
            event.arg1 = arg;
            EventBus.getDefault().post(event);
        }

        private static void postMessage(int what, Object obj) {
            Message event = new Message();
            event.what = what;
            event.obj = obj;
            EventBus.getDefault().post(event);
        }
    }
}
