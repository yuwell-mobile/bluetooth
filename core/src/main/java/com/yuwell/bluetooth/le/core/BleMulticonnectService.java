package com.yuwell.bluetooth.le.core;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanResult;

/**
 * Created by Chen on 2018/4/13.
 */
public class BleMulticonnectService extends Service {

    private static final String TAG = "BleMulticonnectService";
    private static final int MAX_CONNECTIONS = 5;

    private BluetoothAdapter mBluetoothAdapter;
    private BleScanner scanner;

    private Handler mHandler;

    private List<YUBleManager<BleManagerCallbacks>> mBleManagers;

    private @Nullable YUBleManager<BleManagerCallbacks> mCurrentControlManager;

    private final Object mLock = new Object();

    private boolean rescanIfDisconnected = true;
    private BleCallbacksHandler mCallback = new BleCallbacksHandler();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public class LocalBinder extends Binder {

        public BleMulticonnectService getService() {
            return BleMulticonnectService.this;
        }

        public boolean addBleManager(@Nullable YUBleManager<BleManagerCallbacks> manager, BleManagerCallbacks callbacks) {
            recycleManager();

            if (mBleManagers.size() < MAX_CONNECTIONS) {
                mCurrentControlManager = manager;

                if (manager == null) {
                    return true;
                }

                if (mBleManagers.contains(manager)) {
                    if (manager.isConnected()) {
                        return false;
                    } else {
                        scanner.startScan(false);
                        return true;
                    }
                }

                mBleManagers.add(manager);
                mCurrentControlManager.setGattCallbacks(callbacks);
                scanner.startScan(false);
                return true;
            } else {
                Log.d(TAG, "Reached max connections ");
                return false;
            }
        }

        public int getConnectionState() {
            return mCurrentControlManager == null ? BluetoothProfile.STATE_DISCONNECTED : mCurrentControlManager.getConnectionState();
        }

        public void scanBleDevice(boolean autoEnable) {
            scanner.startScan(autoEnable);
        }

        public void stopScanBleDevice() {
            scanner.stopScan();
        }

        public void setIntervals(int scanInterval, int pauseInterval) {
            scanner.setIntervals(scanInterval, pauseInterval);
        }
    }

    @Override
    public void onCreate() {
        // For API level 18 and above, get a reference to BluetoothAdapter
        // through BluetoothManager.
        BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (mBluetoothManager == null) {
            Log.e(TAG, "Unable to initialize BluetoothManager.");
        } else {
            mBluetoothAdapter = mBluetoothManager.getAdapter();
            if (mBluetoothAdapter == null) {
                Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            }
        }

        // Initialize the map of BLE managers
        mBleManagers = new ArrayList<>();
        scanner = new BleScanner(this, scanCallBack);
        mHandler = new Handler(getMainLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        enableBluetoothAdapter();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mBleManagers != null && !mBleManagers.isEmpty()) {
            for (final BleManager<BleManagerCallbacks> manager : mBleManagers) {
                // Service is being destroyed, no need to disconnect manually.
                manager.close();
            }
            mBleManagers.clear();
        }
        mBleManagers = null;

        scanner.stopScan();
        scanner = null;

        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }

        mBluetoothAdapter = null;
        Log.i(TAG, "Service destroyed");
    }

    private void enableBluetoothAdapter() {
        mBluetoothAdapter.enable();
    }

    public class BleCallbacksHandler extends CallbacksHandler.EventBusCallback {

        @Override
        public void onDeviceDisconnected(BluetoothDevice device) {
            if (mCurrentControlManager != null && !mCurrentControlManager.isConnected()) {
                super.onDeviceDisconnected(device);
                restartScanIfNecessary();
            }
            recycleManager();
        }

        @Override
        public void onLinkLossOccurred(BluetoothDevice device) {
            if (mCurrentControlManager != null && !mCurrentControlManager.isConnected()) {
                super.onLinkLossOccurred(device);
                restartScanIfNecessary();
            }
            recycleManager();
        }

        @Override
        public void onDeviceReady(BluetoothDevice device) {
            super.onDeviceReady(device);
            Log.d(TAG, device + " init all done");
        }

        private void restartScanIfNecessary() {
            if (rescanIfDisconnected) {
                scanner.restartScan();
            }
        }
    }

    private final ScanCallback scanCallBack = new ScanCallback() {

        private List<ScanResult> scanResults;

        @Override
        public void onScanResult(final int callbackType, final ScanResult result) {
            // do nothing
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {
            // To avoid dead lock in scanner, do in other method
            this.scanResults = new ArrayList<>(results);

            ScanResult scanResult = null;
			for (ScanResult result : scanResults) {
				Log.d(TAG, "onBatchScanResults(), Device=" + result.getDevice()
						+ ", mDeviceName=" + (result.getScanRecord() != null ? result.getScanRecord().getDeviceName() : result.getDevice().getName())
						+ ", mRssi=" + result.getRssi());
                if (onDeviceScanned(result)) {
                    scanResult = result;
                    break;
                }
			}

            if (scanResult != null) {
                final BluetoothDevice device = scanResult.getDevice();
                synchronized (mLock) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scanner.stopScan();
                        }
                    }, 100);
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mCurrentControlManager != null) {
                                mCurrentControlManager.connect(device)
                                        .useAutoConnect(false)
                                        .retry(3, 100)
                                        .enqueue();
                            }
                        }
                    }, 400);
                }
            }
        }

        @Override
        public void onScanFailed(final int errorCode) {
            // should never be called
        }
    };

    protected boolean onDeviceScanned(ScanResult result) {
        final BluetoothDevice device = result.getDevice();

        return mCurrentControlManager != null && !mCurrentControlManager.isConnected() &&
                mCurrentControlManager.connectable(device, result.getRssi(), result.getScanRecord());
    }

    private void recycleManager() {
        if (mBleManagers.size() == MAX_CONNECTIONS) {
            Iterator<YUBleManager<BleManagerCallbacks>> iterator = mBleManagers.iterator();
            BleManager<BleManagerCallbacks> manager;

            while (iterator.hasNext()) {
                manager = iterator.next();
                if (manager != mCurrentControlManager && !manager.isConnected()) {
                    manager.close();
                    iterator.remove();
                }
            }
        }
    }
}
