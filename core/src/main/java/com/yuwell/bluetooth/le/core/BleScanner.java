package com.yuwell.bluetooth.le.core;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.Keep;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

/**
 * Created by Chen on 2018/4/13.
 */
public class BleScanner {

    private static final String TAG = "BleScanner";

    // Loop scan pause interval
    private int scanInterval = 0;
    private int pauseInterval = 0;

    // Whether it is scanning ble
    private boolean isScanning;
    private boolean isLoopingScanning;

    private Context context;
    private Handler mHandler;
    private BluetoothAdapter mBluetoothAdapter;

    private ScanCallback scanCallBack;

    private List<ScanFilter> mFilters;

    public BleScanner(Context context, ScanCallback scanCallBack) {
        this.context = context.getApplicationContext();
        this.scanCallBack = scanCallBack;

        mHandler = new Handler(context.getMainLooper());

        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            mBluetoothAdapter = bluetoothManager.getAdapter();
        }
    }

    /**
     * Scan interval in seconds
     * @param scanInterval
     * @param pauseInterval
     */
    public void setIntervals(int scanInterval, int pauseInterval) {
        if (scanInterval <= 0 || pauseInterval <= 0)
            throw new IllegalArgumentException("scanInterval and pauseInterval must be > 0");
        this.scanInterval = scanInterval * 1000;
        this.pauseInterval = pauseInterval * 1000;
    }

    public final void startScan(final boolean autoEnable) {
        if (!isLoopingScanning) {
            startScanInternal(mFilters);
            if (mHandler != null) {
                mHandler.removeCallbacksAndMessages(null);
                if (scanInterval > 0) {
                    mHandler.postDelayed(scanSleepRunnable, scanInterval);
                }
            }
            isLoopingScanning = true;
            Log.d(TAG, "Start to scan...");
        }
    }

    public final void restartScan() {
        mHandler.postDelayed(new Runnable() {
            @Keep
            @Override
            public void run() {
                startScan(false);
            }
        }, 500);
    }

    private void startScanInternal(List<ScanFilter> filters) {
        if (!isScanning) {
            if (!isBluetoothEnabled()) { // Auto enable failed, ask the user to open bluetooth adapter manually
                 Toast.makeText(context, "请打开蓝牙后再继续", Toast.LENGTH_SHORT).show();
            } else {
                final BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
                final ScanSettings settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .setReportDelay(1000)
                        .setUseHardwareBatchingIfSupported(false).build();

                scanner.startScan(filters, settings, scanCallBack);
                Log.i(TAG, "Start BLE scan internal");
                isScanning = true;
            }
        } else {
            Log.e(TAG, "Scanner already started");
        }
    }

    public final void stopScan() {
        if (isLoopingScanning) {
            isLoopingScanning = false;
            stopScanInternal();

            if (mHandler != null) {
                mHandler.removeCallbacksAndMessages(null);
            }

            Log.d(TAG, "Stop scanning...");
        }
    }

    public void setFilters(List<ScanFilter> filters) {
        mFilters = filters;
    }

    private void stopScanInternal() {
        if (isScanning) {
            Log.i(TAG, "Stop BLE scan internal");

            try {
                final BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
                scanner.stopScan(scanCallBack);
            } catch (Exception e) {
                Log.d(TAG, "stopScanInternal: " + e);
            }

            isScanning = false;
        } else {
            Log.e(TAG, "Scanner already stopped");
        }
    }

    private boolean isBluetoothEnabled() {
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
    }

    private Runnable scanRunnable = new Runnable() {

    	@Keep
        @Override
        public void run() {
            startScanInternal(mFilters);

            if (mHandler != null && scanInterval > 0) {
                mHandler.postDelayed(scanSleepRunnable, scanInterval);
            }
        }
    };

    private Runnable scanSleepRunnable = new Runnable() {

    	@Keep
        @Override
        public void run() {
            stopScanInternal();

            if (mHandler != null && pauseInterval > 0) {
                mHandler.postDelayed(scanRunnable, pauseInterval);
            }
        }
    };
}
