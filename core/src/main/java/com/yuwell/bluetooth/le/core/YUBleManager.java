package com.yuwell.bluetooth.le.core;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Created by Chen on 2019/6/12.
 */
public abstract class YUBleManager<T extends BleManagerCallbacks> extends BleManager<T> {

	private OnConnectListener onConnectListener;

	/**
	 * The manager constructor.
	 * <p>
	 * After constructing the manager, the callbacks object must be set with
	 * {@link #setGattCallbacks(BleManagerCallbacks)}.
	 * <p>
	 * To connect a device, call {@link #connect(BluetoothDevice)}.
	 *
	 * @param context the context.
	 */
	public YUBleManager(@NonNull Context context) {
		super(context);
	}

	public boolean connectable(final BluetoothDevice device, int rssi, ScanRecord scanRecord) {
		return onConnectListener == null || onConnectListener.connectable(device, rssi, scanRecord);
	}

	public void setOnConnectListener(OnConnectListener onConnectListener) {
		this.onConnectListener = onConnectListener;
	}

	public interface OnConnectListener {
		boolean connectable(final BluetoothDevice device, int rssi, ScanRecord scanRecord);
	}

	@Override
	public void log(int priority, @NonNull String message) {
		Log.println(priority, "BleManager", message);
	}
}
