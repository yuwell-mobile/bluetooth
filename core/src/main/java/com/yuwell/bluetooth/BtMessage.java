package com.yuwell.bluetooth;

/**
 * Bluetooth 2.0 event messages
 * Created by Chen on 2015/11/21.
 */
public interface BtMessage {

    int STATE_CHANGE = 0x1200;

    int CONNECTION_FAILED = 0x1201;

    int CONNECTION_LOST = 0x1202;

    int ECG_DATA = 0x1203;
}
