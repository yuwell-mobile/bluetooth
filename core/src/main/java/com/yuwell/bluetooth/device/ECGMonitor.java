package com.yuwell.bluetooth.device;

import android.os.Message;

import com.yuwell.bluetooth.BtMessage;
import com.yuwell.bluetooth.OnDataRead;
import com.yuwell.bluetooth.WriteData;
import com.yuwell.bluetooth.databean.ECGData;
import com.yuwell.bluetooth.utils.Utils;

import org.greenrobot.eventbus.EventBus;

/**
 * ECG monitor behavior
 * Created by Chen on 2015/11/21.
 */
public class ECGMonitor implements OnDataRead {

    public static final String DEVICE_NAME = "ECG:HC-201B";

    private static final byte[] ACK_HELLO = {85, 01, 01, 42 | -128, 10};
    private static final byte[] ACK_TOTAL_LENGTH = {85, 2, 0, 0, 10};

    private int n;
    private int timeN = 0;
    private byte packageN = 0;
    private int packageLen = 30;
    private byte temp1;
    private byte byteLenH = 0;
    private byte byteLenL = 0;
    private byte[] dataArray;

    private WriteData writeData;

    public ECGMonitor(WriteData writeData) {
        this.writeData = writeData;
    }

    @Override
    public void onRead(int len, byte[] data) {
        n++;

        if (packageN == 0 && timeN == 0 && n == 1) {
            dataArray = new byte[28];
        }

        addData(data[0]);
        if (timeN == 3 && n == 9) {
            byteLenL = data[0];
        }
        if (timeN == 3 && n == 10) {
            byteLenH = data[0];
        }

        if (timeN == 3 && n == 2) {
            temp1 = data[0];
        }
        if (timeN == 3 && n == 11) {
            //长度
            packageLen = Utils.unsignedBytesToInt(byteLenL, byteLenH) + 10;
        }

        if (len > 0) {
            if (n == 4 && data[0] == 85 && timeN == 0) {
                for (byte b : ACK_HELLO) {
                    write(b);
                }
                n = 0;
                timeN = 1;
            }
            if (data[0] == 2 && timeN == 1) {
                for (byte b : ACK_TOTAL_LENGTH) {
                    write(b);
                }
                timeN = 2;
            }
            if ((data[0] == 3 || data[0] == 4) && timeN == 2) {
                n = 2;
                timeN = 3;
                temp1 = data[0];
                packageN = 0;
            }
            if (n == packageLen && timeN == 3) {
                packageN++;
                // ACK-Data包
                byte[] b1 = {85, temp1, packageN, 0, 10};
                for (byte b : b1) {
                    write(b);
                }
                n = 0;
                if (packageN == 30) {
                    timeN = 0;
                    packageN = 0;
                    decodeData();
                }
            }
        }
    }

    private void addData(byte b) {
        if (packageN == 0 && n > 10 && n < 39) {
            dataArray[n - 11] = b;
        }
    }

    private void decodeData() {
        ECGData ecgData = new ECGData(dataArray);
        Message message = new Message();
        message.what = BtMessage.ECG_DATA;
        message.obj = ecgData;
        EventBus.getDefault().post(message);
    }

    private void write(byte b) {
        if (writeData != null) {
            writeData.write(b);
        }
    }
}
