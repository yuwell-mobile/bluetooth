package com.yuwell.bluetooth;

/**
 * On data comes interface
 * Created by Chen on 2015/8/26.
 */
public interface OnDataRead {
    void onRead(int dataSize, byte[] data);
}
