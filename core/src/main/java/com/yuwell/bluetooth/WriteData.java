package com.yuwell.bluetooth;

/**
 * Created by chenshuai on 2015/11/21.
 */
public interface WriteData {

    void write(byte b);
}
