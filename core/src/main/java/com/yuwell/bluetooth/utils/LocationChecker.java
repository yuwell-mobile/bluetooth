package com.yuwell.bluetooth.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;

import com.yuwell.bluetooth.R;

/**
 * Created by Chen on 2019/4/23.
 */
public class LocationChecker {

	private static final String TAG = "LocationChecker";

	@TargetApi(Build.VERSION_CODES.P)
	public static boolean isLocationEnabled(Context context) {
		LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		return mLocationManager != null && mLocationManager.isLocationEnabled();
	}

	public static void showAlertDialogIfNotEnabled(final Activity activity) {
		LocationManager mLocationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

		if (mLocationManager != null) {
			boolean enabled = false;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
				enabled = mLocationManager.isLocationEnabled();
			} else {
				try {
					enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
							mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
				} catch (Exception e) {
					Log.d(TAG, "isProviderEnabled: " + e);
				}
			}

			if (!enabled) {
				final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				builder.setMessage(R.string.location_service_not_enabled)
						.setCancelable(false)
						.setPositiveButton("开启", new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {
								activity.startActivity(new Intent(
										android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
							}
						})
						.setNegativeButton("取消", new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {
								dialog.cancel();
							}
						});
				final AlertDialog alert = builder.create();
				alert.show();
			}
		}
	}
}
