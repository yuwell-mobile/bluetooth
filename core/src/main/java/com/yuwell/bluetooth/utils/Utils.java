package com.yuwell.bluetooth.utils;

import java.math.BigDecimal;

/**
 * Common utilities
 * Created by chenshuai on 2015/9/9.
 */
public class Utils {

    public static int decimalToHex(int n) {
        return Integer.parseInt(Integer.toHexString(n), 16);
    }

    public static float multiply(float v1, float v2) {
        return multiply(v1, v2, 1);
    }

    /**
     * 截断指定位数小数
     * @param v1
     * @param v2
     * @param scale
     * @return
     */
    public static float multiply(float v1, float v2, int scale) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return b1.multiply(b2).setScale(scale, BigDecimal.ROUND_DOWN).floatValue();
    }

    public static float scaleDown(int scale, float f) {
        return new BigDecimal(f).setScale(scale, BigDecimal.ROUND_DOWN).floatValue();
    }

    public static double scaleDown(int scale, double d) {
        return new BigDecimal(d).setScale(scale, BigDecimal.ROUND_DOWN).doubleValue();
    }

    public static double roundHalfUpWithScale1(double value) {
        BigDecimal b = new BigDecimal(value);
        return b.setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static String retainOneDecimal(String val) {
        if (val.endsWith(".")) {
            val = val.substring(0, val.length() - 1);
        }
        String[] strArr = val.split("\\.");
        if (strArr.length == 2 && strArr[1].length() > 1) {
            val = strArr[0] + "." + strArr[1].substring(0, 1);
        }
        return val;
    }

    public static String byteToBit(byte b) {
        return "" +(byte)((b >> 7) & 0x1) +
                (byte)((b >> 6) & 0x1) +
                (byte)((b >> 5) & 0x1) +
                (byte)((b >> 4) & 0x1) +
                (byte)((b >> 3) & 0x1) +
                (byte)((b >> 2) & 0x1) +
                (byte)((b >> 1) & 0x1) +
                (byte)((b >> 0) & 0x1);
    }

    public static int bitToInt(String s) {
        return Integer.valueOf(s, 2);
    }

    public static int unsignedBytesToInt(byte b0, byte b1) {
        return (unsignedByteToInt(b0) + (unsignedByteToInt(b1) << 8));
    }

    public static int unsignedByteToInt(byte b) {
        return b & 0xFF;
    }

    public static int unsignedToSigned(int unsigned, int size) {
        if ((unsigned & (1 << size - 1)) != 0) {
            unsigned = -1 * ((1 << size - 1) - (unsigned & ((1 << size - 1) - 1)));
        }
        return unsigned;
    }
}
