package com.yuwell.bluetooth.databean;

import com.yuwell.bluetooth.utils.Utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Chen on 2015/11/12.
 */
public class ECGData {

    private Date date;
    private float heartRate;
    private float pR;
    private float QT;
    private float rV;
    private float pV;
    private float tV;
    private float stV;
    private String stAssessment;
    private String arrhythima;
    private String waveQuality;
    private String heartRateAssessment;

    public ECGData() {
        this.date = new Date();
        this.stAssessment = "00";
        this.arrhythima = "0";
        this.waveQuality = "0";
        this.heartRateAssessment = "000";
    }

    public ECGData(byte[] bytes) {
        int offset = 0;
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Utils.unsignedByteToInt(bytes[0]) + 2000);
        calendar.set(Calendar.MONTH, Utils.unsignedByteToInt(bytes[1]) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Utils.unsignedByteToInt(bytes[2]));
        calendar.set(Calendar.HOUR_OF_DAY, Utils.unsignedByteToInt(bytes[3]));
        calendar.set(Calendar.MINUTE, Utils.unsignedByteToInt(bytes[4]));
        calendar.set(Calendar.SECOND, Utils.unsignedByteToInt(bytes[5]));
        this.date = calendar.getTime();
        offset += 7;

        String result = Utils.byteToBit(bytes[offset]);
        this.stAssessment = result.substring(0, 2);
        this.arrhythima = result.substring(2, 3);
        this.waveQuality = result.substring(3, 4);
        this.heartRateAssessment = result.substring(4, 7);
        offset += 7;

        this.heartRate = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]) / 10f;
        offset += 2;
        this.pR = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]) * 2;
        offset += 2;
        this.QT = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]) * 2;
        offset += 2;
        this.rV = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]) / 1000f;
        offset += 2;
        this.pV = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]) / 1000f;
        offset += 2;
        this.tV = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]) / 1000f;
        offset += 2;
        this.stV = Utils.unsignedBytesToInt(bytes[offset], bytes[offset + 1]) / 1000f;
    }

    public Date getDate() {
        return date;
    }

    public float getHeartRate() {
        return heartRate;
    }

    public float getpR() {
        return pR;
    }

    public float getQT() {
        return QT;
    }

    public float getrV() {
        return rV;
    }

    public float getpV() {
        return pV;
    }

    public float gettV() {
        return tV;
    }

    public float getStV() {
        return stV;
    }

    public String getStAssessment() {
        return stAssessment;
    }

    public String getArrhythima() {
        return arrhythima;
    }

    public String getWaveQuality() {
        return waveQuality;
    }

    public String getHeartRateAssessment() {
        return heartRateAssessment;
    }

    public void setHeartRate(float heartRate) {
        this.heartRate = heartRate;
    }

    public void setpR(float pR) {
        this.pR = pR;
    }

    public void setQT(float QT) {
        this.QT = QT;
    }

    public void setrV(float rV) {
        this.rV = rV;
    }

    public void setpV(float pV) {
        this.pV = pV;
    }

    public void settV(float tV) {
        this.tV = tV;
    }

    public void setStV(float stV) {
        this.stV = stV;
    }

    @Override
    public String toString() {
        String str = "心率：" + heartRate + "\n";
        str += "ST段：" + stV + "\n";
        str += "PR间期：" + pR + "ms\n";
        str += "QT间期：" + QT + "ms\n";
        str += "P波：" + pV + "mv\n";
        str += "R波：" + rV + "mv\n";
        str += "T波：" + tV + "mv\n";
        return str;
    }
}
