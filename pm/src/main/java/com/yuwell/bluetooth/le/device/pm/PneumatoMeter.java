package com.yuwell.bluetooth.le.device.pm;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.contec.sp.code.bean.Parameter;
import com.contec.sp.code.bean.ResultData;
import com.contec.sp.code.callback.CommunicateCallback;
import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.core.YUBleManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Created by Chen on 2019/7/23.
 */
public class PneumatoMeter extends YUBleManager<PneumatorMeterCallbacks> {

	private static final String TAG = "PneumatoMeter";

	private BluetoothGattCharacteristic characteristicToWrite = null;
	private BluetoothGattCharacteristic notifyCharacteristic = null;

	private static PneumatoMeter managerInstance = null;

	private com.contec.sp.code.a.c parser = new com.contec.sp.code.a.c();
	private AtomicBoolean mUserAction = new AtomicBoolean(false);
	private Handler mHandler = new Handler();
	private Parameter parameter;

	private PneumatoMeter(@NonNull Context context) {
		super(context);
	}

	public static synchronized PneumatoMeter getInstance(final Context context) {
		return new PneumatoMeter(context);
	}

	@SuppressLint("MissingPermission")
	@Override
	public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
		String name = device.getName();
		if (!TextUtils.isEmpty(name)) {
			return name.startsWith("PULMO80B");
		}
		return false;
	}

	public void readData() {
		parser.syncData();
		mUserAction.set(true);
	}

	public void setParameter(boolean male, int age, int height) {
		parameter = new Parameter();
		parameter.setMeatureMode(Parameter.MeatureMode.ALLMODE);
		parameter.setUserSex(male ? Parameter.UserParametersSex.MAN : Parameter.UserParametersSex.WOMAN);
		parameter.setUserAge(age);
		parameter.setUserWeight(0);
		parameter.setUserHight(height);
		parameter.setUserSmoke(Parameter.UserParametersSmoke.NOSMOKE);
		parser.setUserParameter(parameter);
		bcm.a(com.contec.sp.code.a.d.a(parameter));
	}

	@NonNull
	@Override
	protected BleManagerGattCallback getGattCallback() {
		return mGattCallback;
	}

	private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

		@Override
		protected void initialize() {
			super.initialize();

			parser.setDevice(getBluetoothDevice());
			parser.setCommunicationManager(bcm);
			parser.connect(mCommunicateCallback);

			setNotificationCallback(notifyCharacteristic).with(new DataReceivedCallback() {
				@Override
				public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
					if (mUserAction.get() && data.getValue() != null) {
						parser.onDataReceived(data.getValue());
					}
				}
			});

			enableNotifications(notifyCharacteristic).enqueue();
			if (parameter != null) {
				writeCharacteristic(characteristicToWrite, com.contec.sp.code.a.d.a(parameter)).enqueue();
			}
		}

		@Override
		protected boolean isRequiredServiceSupported(@NonNull BluetoothGatt gatt) {
			BluetoothGattService mUrineService = gatt.getService(Service.URINE_CONTEC);
			if (mUrineService != null) {
				characteristicToWrite = mUrineService.getCharacteristic(Characteristic.URINE_CONTEC_WRITE);
				notifyCharacteristic = mUrineService.getCharacteristic(Characteristic.URINE_CONTEC_NOTIFY);
			}
			return mUrineService != null;
		}

		@Override
		protected void onDeviceDisconnected() {
			characteristicToWrite = null;
			notifyCharacteristic = null;
		}
	};

	private CommunicateCallback mCommunicateCallback = new CommunicateCallback() {
		@Override
		public void onCommunicateSuccess(ResultData data) {
			if (!mUserAction.get()) {
				return;
			}

			if (data.sp80BDataBeans.size() == 0) {
				mUserAction.set(false);
				return;
			}

			final List<PneumatorData> dataList = new ArrayList<>();

			PneumatorData pd;
			for (ResultData.SP80BDataBean bean : data.sp80BDataBeans) {
				Parameter.UserParametersSex sex = bean.getGender() == 0 ?
						Parameter.UserParametersSex.MAN :
						Parameter.UserParametersSex.WOMAN;

				if (parameter != null && (parameter.getUserAge() != bean.getAge() ||
						parameter.getUserHight() != bean.getHeight() ||
						parameter.getUserSex() != sex)) {
					continue;
				}

				pd = new PneumatorData();
				pd.standard = bean.getStandard();
				pd.age = bean.getAge();
				pd.height = bean.getHeight();
				pd.weight = bean.getWeight();
				pd.gender = bean.getGender();
				pd.drug = bean.getDrug();
				pd.measureType = bean.getMeatureType();
				pd.FEV1 = bean.getFEV1();
				pd.FEV1_FVC = bean.getFEV1_FVC();
				pd.FVC = bean.getFVC();
				pd.PEF = bean.getPEF();
				pd.predFEV1 = data.getEstimate_FEV1();
				pd.predFEV1_FVC = data.getEstimate_FEV1_FVC();
				pd.predFVC = data.getEstimate_FVC();
				pd.predPEF = data.getEstimate_PEF();

				Calendar calendar = Calendar.getInstance();
				calendar.set(bean.getYear(), bean.getMonth() - 1, bean.getDay(), bean.getHour(),
						bean.getMinute(), bean.getSecond());
				calendar.set(Calendar.MILLISECOND, 0);
				pd.time = calendar.getTime();

				dataList.add(pd);
			}

			if (dataList.size() > 0) {
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						mCallbacks.onDataRead(dataList);
					}
				});
			}

			mUserAction.set(false);
		}

		@Override
		public void onCommunicateFailed(int i) {

		}

		@Override
		public void onCommunicateProgress(int i) {
			Log.v(TAG, "onCommunicateProgress() returned: " + i);
		}
	};

	private com.contec.sp.code.base.a bcm = new com.contec.sp.code.base.a() {
		@Override
		public void a(BluetoothDevice bluetoothDevice, CommunicateCallback communicateCallback) {
			// connect
		}

		@Override
		public void a() {
			// disconnect
		}

		@Override
		public void a(byte[] bytes) {
			if (isConnected()) {
				writeCharacteristic(characteristicToWrite, bytes).enqueue();
			}
		}

		@Override
		public byte[] b() {
			return new byte[0];
		}
	};
}
