package com.yuwell.bluetooth.le.device.pm;

import java.util.List;

import no.nordicsemi.android.ble.BleManagerCallbacks;

/**
 * Created by Chen on 2019/7/23.
 */
public interface PneumatorMeterCallbacks extends BleManagerCallbacks {

	void onDataRead(List<PneumatorData> list);
}
