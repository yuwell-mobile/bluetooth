package com.yuwell.bluetooth.le.device.pm;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chen on 2019/7/24.
 */
public class PneumatorData {

	public static final int GENDER_MALE = 0;
	public static final int GENDER_FEMALE = 1;

	public static final int DRUG_BEFORE = 0;
	public static final int DRUG_AFTER = 1;

	public Date time;
	public String measureType;
	public int gender;
	public int age;
	public int height;
	public int weight;
	public String standard;
	public int drug;
	public String FVC;
	public String FEV1;
	public String PEF;
	public String FEV1_FVC;
	public String predFVC;
	public String predFEV1;
	public String predPEF;
	public String predFEV1_FVC;

	@NonNull
	@Override
	public String toString() {
		Map<String, Object> map = new HashMap<>();
		map.put("time", time);
		map.put("gender", gender);
		map.put("age", age);
		map.put("height", height);
		map.put("weight", weight);
		map.put("FVC", FVC);
		map.put("FEV1", FEV1);
		map.put("PEF", PEF);
		map.put("FEV1/FVC", FEV1_FVC);
		map.put("PRED_FVC", predFVC);
		map.put("PRED_FEV1", predFEV1);
		map.put("PRED_PEF", predPEF);
		map.put("PRED_FEV1_FVC", predFEV1_FVC);

		return new JSONObject(map).toString();
	}
}
