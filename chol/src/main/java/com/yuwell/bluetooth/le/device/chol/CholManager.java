package com.yuwell.bluetooth.le.device.chol;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.support.annotation.NonNull;

import com.yuwell.bluetooth.le.constants.Characteristic;
import com.yuwell.bluetooth.le.constants.Service;
import com.yuwell.bluetooth.le.core.YUBleManager;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.support.v18.scanner.ScanRecord;

/**
 * Cholesterol meter behavior
 * Created by Chen on 2015/9/16.
 */
public class CholManager extends YUBleManager<CholManagerCallbacks> {

    private static final String TAG = CholManager.class.getSimpleName();

    private BluetoothGattCharacteristic writeCharacteristic = null;
    private BluetoothGattCharacteristic readCharacteristic = null;

    private int num = 0;
    private StringBuilder builder;

    private static CholManager mInstance;

    /**
     * Returns the singleton implementation of GlucoseManager
     */
    public static CholManager getInstance(final Context context) {
        return new CholManager(context);
    }

    private CholManager(Context context) {
        super(context);
    }

    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean connectable(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
        return "iGate".equals(device.getName());
    }

    public void readCholData() {
        if (isConnected()) {
            num = 0;
            builder = new StringBuilder();

            writeCharacteristic(writeCharacteristic, Data.from("GET")).enqueue();
        }
    }

    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(readCharacteristic).with(new DataReceivedCallback() {
                @Override
                public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                    byte[] bytes = data.getValue();
                    String strData;
                    strData = new String(bytes, StandardCharsets.UTF_8);

                    builder.append(strData);
                    if (builder.toString().endsWith("\r\n\r\n")) {
                        String dataStr = builder.toString().replaceAll("null", "");

                        CholesterolData cholesterolData = new CholesterolData();

                        String unit = dataStr.contains("mg/dL") ? "mg/dL" : "mmol/L";
                        cholesterolData.unit = unit;

                        String[] dataArray = dataStr.split("\r\n");
                        try {
                            cholesterolData.time = sdf.parse(dataArray[0].replaceAll("\\(Y-M-D\\)",
                                    ""));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String[] subData;
                        String value;
                        for (int i = 2; i < dataArray.length; i++) {
                            subData = dataArray[i].split(":");
                            if (subData.length == 2) {
                                value = subData[1].replace(unit, "").trim();

                                switch (subData[0]) {
                                    case "CHOL":
                                        cholesterolData.chol = value;
                                        break;
                                    case "HDL":
                                        cholesterolData.hdl = value;
                                        break;
                                    case "TRIG":
                                        cholesterolData.trig = value;
                                        break;
                                    case "LDL":
                                        cholesterolData.ldl = value;
                                        break;
                                }
                            }
                        }

                        mCallbacks.onCholRead(device, cholesterolData);
                    }
                }
            });

            enableNotifications(readCharacteristic).enqueue();
        }

        @Override
        protected boolean isRequiredServiceSupported(BluetoothGatt gatt) {
            BluetoothGattService mCholService = gatt.getService(Service.CHOLESTEROL);

            if (mCholService != null) {
                List<BluetoothGattCharacteristic> characteristics = mCholService.getCharacteristics();
                for (BluetoothGattCharacteristic characteristic : characteristics) {
                    if (characteristic.getUuid().equals(Characteristic.CHOLESTEROL_READ)) {
                        if (characteristic.getProperties() == BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) {
                            writeCharacteristic = characteristic;
                        } else {
                            readCharacteristic = characteristic;
                        }
                    }
                }
            }

            return mCholService != null;
        }

        @Override
        protected void onDeviceDisconnected() {
            readCharacteristic = null;
            writeCharacteristic = null;
        }

    };
}
