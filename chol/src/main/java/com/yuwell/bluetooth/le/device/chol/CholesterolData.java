package com.yuwell.bluetooth.le.device.chol;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chen on 2018/5/31.
 */
public class CholesterolData {

	public String unit;
	public String chol;
	public String hdl;
	public String trig;
	public String ldl;
	public Date time;

	public String toJsonString() {
		Map<String, Object> map = new HashMap<>();
		map.put("unit", unit);
		map.put("chol", chol);
		map.put("hdl", hdl);
		map.put("trig", trig);
		map.put("ldl", ldl);
		map.put("time", time == null ? new Date().getTime() : time.getTime());

		return new JSONObject(map).toString();
	}
}
